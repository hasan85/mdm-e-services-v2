import {combineReducers} from "redux";
import notification from "./reducers/notification";
import user from "./reducers/user";
import loader from "./reducers/loader";
import accounts from "./reducers/accounts";
import socket from "./reducers/socket";
import socketId from "./reducers/socketId";

export default combineReducers({
    user,
    notification,
    loader,
    accounts,
    socket,
    socketId
});