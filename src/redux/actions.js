import {
    NOTIFICATION_HIDE,
    NOTIFICATION_SUCCESS,
    NOTIFICATION_ERROR,
    NOTIFICATION_INFO,
    USER_LOGIN,
    USER_LOGOUT,
    SHOW_LOADER,
    HIDE_LOADER,
    USER_UPDATE,
    INIT_ACCOUNTS,
    UPDATE_ACCOUNTS,
    REMOVE_ACCOUNTS,
    INIT_SOCKET, INIT_SOCKET_ID
} from "./actionTypes";

export const successNotification = payload => ({
    type: NOTIFICATION_SUCCESS,
    payload
});

export const errorNotification = payload => ({
    type: NOTIFICATION_ERROR,
    payload
});

export const infoNotification = payload => ({
    type: NOTIFICATION_INFO,
    payload
});

export const hideNotification = () => ({
    type: NOTIFICATION_HIDE,
    payload: false
});

export const loginUser = user => ({
    type: USER_LOGIN,
    user
});

export const updateUser = payload => ({
    type: USER_UPDATE,
    payload
});


export const logoutUser = () => ({
    type: USER_LOGOUT
});

export const showLoader = text => ({
    type: SHOW_LOADER,
    text
});

export const hideLoader = () => ({
    type: HIDE_LOADER
});

export const initAccounts = accounts => ({
    type: INIT_ACCOUNTS,
    accounts
});
export const updateAccounts = payload => ({
    type: UPDATE_ACCOUNTS,
    payload
});
export const removeAccounts = () => ({
    type: REMOVE_ACCOUNTS
});
export const initSocket = socket => ({
    type: INIT_SOCKET,
    socket
});

export const setSocketId = socketId => ({
    type: INIT_SOCKET_ID,
    socketId
});