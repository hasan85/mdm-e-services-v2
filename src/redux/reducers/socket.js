import {INIT_SOCKET} from "../actionTypes";

const INITIALSTATE = null;
const socket = (state = INITIALSTATE, action) => {
    switch (action.type) {
        case INIT_SOCKET:
            const {socket} = action;
            window.socket = socket;
            return socket;
        default:
            return state;
    }
};

export default socket;