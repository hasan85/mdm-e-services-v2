import {INIT_SOCKET_ID} from "../actionTypes";

const INITIALSTATE = "";
const socketId = (state = INITIALSTATE, action) => {
    switch (action.type) {
        case INIT_SOCKET_ID:
            const {socketId} = action;
            window.socketId = socketId;
            return socketId;
        default:
            return state;
    }
};

export default socketId;