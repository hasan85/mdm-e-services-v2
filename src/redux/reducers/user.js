import {USER_LOGIN, USER_LOGOUT, USER_UPDATE} from '../actionTypes';
import {setCookie} from "../../tools/config";

const initialState = null;

const user = (state = initialState, action) => {
    switch (action.type) {
        case USER_LOGIN:
            return action.user;
        case USER_UPDATE:
            const _user = typeof (action.payload) === "string"
                ? {...state, ...JSON.parse(action.payload)}
                : {...state, ...action.payload};
            return _user;
        case USER_LOGOUT:
            setCookie(["token", ""], "/", new Date(), {allowNull: true});
            return null;
        default:
            return state;
    }
};

export default user;