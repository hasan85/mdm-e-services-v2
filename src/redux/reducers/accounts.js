import {INIT_ACCOUNTS, UPDATE_ACCOUNTS, REMOVE_ACCOUNTS} from "../actionTypes"
const initialState = null;

const accounts = (state = initialState, action) => {
    switch (action.type) {
        case INIT_ACCOUNTS:
            return action.accounts;
        case UPDATE_ACCOUNTS:
            return {...state, ...action.payload};
        case REMOVE_ACCOUNTS:
            return initialState;
        default:
            return state;
    }
};
export default accounts;