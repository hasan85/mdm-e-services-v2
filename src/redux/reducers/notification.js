import {NOTIFICATION_SUCCESS, NOTIFICATION_ERROR, NOTIFICATION_HIDE, NOTIFICATION_INFO} from '../actionTypes';

const initialState = {status: false};

const notification = (state = initialState, action) => {
    let text = "";
    switch (action.type) {
        case NOTIFICATION_SUCCESS:
            text = action.payload.text || "";
            return {status: true, type: "success", text, callback: action.payload.callback};
        case NOTIFICATION_ERROR:
            text = action.payload.text || "";
            return {status: true, type: "error", text, callback: action.payload.callback};
        case NOTIFICATION_INFO:
            text = action.payload.text || "";
            return {status: true, type: "info", text, callback: action.payload.callback};
        case NOTIFICATION_HIDE:
            return initialState;
        default:
            return state;
    }
};

export default notification;