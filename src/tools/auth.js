import {parseJwt, config, postData} from "./config";
import _ from 'lodash';

export const authenticateUser = async (token: string): void => new Promise(
    async (resolve, reject): void => {
        const _resp = new Response();
        if (isTokenExpired(token)) {
            _resp.statusCode = 4;
            _resp.statusMessage = "Token is Expired";
            _resp.data = null;
            resolve(_resp);
        } else {
            try {
                const resp = await postData({token}, config.checkContextUrl);
                if (resp) {
                    if (resp.code === 0) {
                        _resp.statusCode = 0;
                        _resp.statusMessage = "OK";
                        _resp.responseData = {...resp.data, token: resp.token};
                    } else if (resp.code === 2) {
                        _resp.statusCode = 6;
                        _resp.statusMessage = resp.message;
                        _resp.responseData = null;
                    }
                } else {
                    _resp.statusCode = 500;
                    _resp.statusMessage = "Server xətası";
                }
                resolve(_resp);
            } catch (e) {
                reject(e)
            }
        }
    });

export const loginUser = async (credentials: object): void => new Promise(
    async (resolve, reject): void => {
        const res = new Response();
        const resp = await postData(credentials, config.asanLoginUrl);
        if (resp)
            if (resp.code === 0) {
                res.statusCode = 0;
                res.statusMessage = "OK";
                res.responseData = resp.user;
                resolve(res);
            } else {
                res.statusCode = resp.code;
                res.statusMessage = resp.data.messageAz;
                res.data = null;
                reject(res);
            }
        else
            reject()
    });


export const logoutUser = async() => {
    const resp = await fetch(config.logoutUrl);
    const data = await resp.json();
};

/*
* Checks token expires date
* */
export const isTokenExpired = token => {
    if (token) {
        const tokenData = parseJwt(token);
        if (!tokenData)
            return true;
        const exp_date = new Date(tokenData.exp * 1000);
        const now = new Date();
        return now > exp_date;
    } else
        return true;
};

export class Response {
    constructor(code, message, data, etc) {
        this.statusCode = code;
        this.statusMessage = message;
        this.responseData = data;
        if (!_.isEmpty(etc)) {
            etc.forEach(param => this[Object.keys(param)[0]] = param);
        }
    }

}