import {createMuiTheme} from '@material-ui/core/styles';


//noinspection JSCheckFunctionSignatures
export const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#1976D2",
            contrastText: "#fff"
        },
        secondary: {
            main: "#fff",
            contrastText: '#b8c7ca'
        },
        accent: {
            main: "#5e9cd3"
        },
        tableColor: {
            main: "#258af7"
        }
    },
    typography: {
        fontSize: 12,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(',')
    }
});
