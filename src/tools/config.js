import _ from "lodash";
import path from "path";
import {array, object} from "prop-types";

export const config = {
    root: path.resolve(__dirname, "../"),
    port: process.env.PORT || 8080,
    ip: "192.168.31.14",
    asanLoginUrl: "/asan_imza_login/",
    checkContextUrl: "/get_context/",
    logoutUrl: "/logout/",
    getUserParametersURL: "/get_user_parameters/",
    getAccountURL: "/get_account/",
    getAccountStatementURL: "/get_account_statement/",
    getAccountTransactionURL: "/get_account_transaction/",
    thriftClientPort: 55557,
    thriftEServicePort: 55558,
    thriftServerPort: 9095,
    secrets: {
        session: "qaw9d8qwdas9udas-0d9asjadja90hasdasdh"
    }
};

export const demoUser = {
    status: "demo",
    context: {
        contexts: [{
            contextPerson: {
                id: 12310,
                nameAz: "Qasımova Sevinc Rəhim qızı",
                nameEn: null,
                code: "NAME"
            },
            id: 87082,
            role: {
                id: 178,
                nameAz: "İnvestor",
                nameEn: "Shareholder",
                code: "ROLE_SHAREHOLDER"
            }
        }],
        person: {
            nameAz: "Demo"
        }
    }
};

export const parseJwt = token => {
    const base64Url = token.split('.')[1];
    const base64 = base64Url && base64Url.replace(/-/g, '+').replace(/_/g, '/');
    if (!base64) return null;
    return JSON.parse(window.atob(base64));
};

export const postData = async (credentials, url) => {
    try {
        const response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(credentials)
        });
        const body = await response.json();
        if (response.status !== 200) throw Error(body.message);
        return body;
    } catch (e) {
        console.log(e);
    }
};

export const getContext = async (credentials, url) => {
    try {
        const response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(credentials)
        });
        if (response) {
            const body = await response.json();
            if (response.status !== 200) throw Error(body.message);
            return body;
        } else {
            throw Error("404 NOT FOUND!");
        }
    } catch (e) {
        console.log(e);
    }
};

export const getCookieByName = name => {
    const cookies = document.cookie.split(";");
    const filteredCook = cookies.filter(cookie => {
        const c = cookie.trim();
        if (c) {
            const keyValue = c.split("=");
            const cookieToken = {[keyValue[0]]: keyValue[1]};

            if (cookieToken) {
                if (cookieToken[name])
                    return cookieToken[name];
                else return null;
            } else return null;

        } else {
            return null;
        }
    });
    if (filteredCook.length > 0) {
        const keyValueArray = filteredCook[0].split('=');
        if (keyValueArray.length === 2)
            return keyValueArray[1].trim();
        else return null;
    } else return null;
};

export const setCookie = (keyValue: array, path: string, expireDate: date, options: object):object => {
    if (_.isEmpty(keyValue) && keyValue.length !== 2 && !(options && options.allowNull)) return null;
    let new_cookie = `${keyValue[0]}=${keyValue[1]}`;
    new_cookie = path ? `${new_cookie};path=${path}` : `${new_cookie}`;
    new_cookie = new_cookie[new_cookie.length - 1] === ";" ? `${new_cookie}` : `${new_cookie};`;
    new_cookie = expireDate ? `${new_cookie};expires=${expireDate}` : `${new_cookie}`;
    new_cookie = new_cookie[new_cookie.length - 1] === ";" ? `${new_cookie}` : `${new_cookie};`;
    document.cookie = new_cookie;
    return {status: true, new_cookie};
};

export const getTokenExpireDate = daysToExpire => {
    const now = new Date();
    const currentDate = now.getDate();
    const expireDate = currentDate + daysToExpire;
    now.setDate(expireDate);
    return new Date(now).toUTCString();
};

export const textFilterParams = {
    filterOptions: [
        {
            displayKey: 'Equal',
            displayName: 'Bərabərdir',
            test: (filterValue, cellValue) => cellValue === filterValue
        },
        {
            displayKey: 'NotEqual',
            displayName: 'Bərabər deyil',
            test: (filterValue, cellValue) => cellValue !== filterValue
        },
        {
            displayKey: 'Contain',
            displayName: 'Daxil',
            test: (filterValue, cellValue) => cellValue.includes(filterValue)
        },
        {
            displayKey: 'NotContain',
            displayName: 'Daxil deyil',
            test: (filterValue, cellValue) => !cellValue.includes(filterValue)
        }
    ],
    debounceMs: 0,
    caseSensitive: false,
    suppressAndOrCondition: true
};

export const numberFilterParams = {
    filterOptions: [
        {
            displayKey: 'Equal',
            displayName: 'Bərabərdir',
            test: (filterValue, cellValue) => {
                return parseFloat(cellValue) === parseFloat(filterValue);
            }
        },
        {
            displayKey: 'NotEqual',
            displayName: 'Bərabər deyil',
            test: (filterValue, cellValue) => {
                return parseFloat(cellValue) !== parseFloat(filterValue);
            }
        },
        {
            displayKey: 'LessThan',
            displayName: 'Daxil olunan dəyərdən kiçik',
            test: (filterValue, cellValue) => {
                return parseFloat(cellValue) <= parseFloat(filterValue);
            }
        },
        {
            displayKey: 'GreaterThan',
            displayName: 'Daxil olunan dəyərdən böyük',
            test: (filterValue, cellValue) => {
                return parseFloat(cellValue) >= parseFloat(filterValue);
            }
        }
    ],
    debounceMs: 0,
    caseSensitive: false,
    suppressAndOrCondition: true
};

export const dateFilterParams = {
    filterOptions: [
        {
            displayKey: 'LessThan',
            displayName: 'Daxil olunan tarixə qədər',
            test: (filterValue, cellValue) => {
                return filterValue >= new Date(cellValue);
            }
        },
        {
            displayKey: 'GreaterThan',
            displayName: 'Daxil olunan tarixdən sonra',
            test: (filterValue, cellValue) => {
                return filterValue <= new Date(cellValue);
            }
        },
        {
            displayKey: 'Equal',
            displayName: 'Bərabər',
            test: (filterValue, cellValue) => {
                return filterValue >= new Date(cellValue);
            }
        },
        {
            displayKey: 'NotEqual',
            displayName: 'Bərabər deyil',
            test: (filterValue, cellValue) => {
                return filterValue <= new Date(cellValue);
            }
        }
    ],
    debounceMs: 0,
    caseSensitive: false,
    suppressAndOrCondition: true
};

window.getCookieByName = getCookieByName;
window.setCookie = setCookie;