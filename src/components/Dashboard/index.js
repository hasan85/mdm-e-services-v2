import React, {Component} from 'react';
import {connect} from 'react-redux';
import withStyles from "@material-ui/core/styles/withStyles";
import {Typography} from "@material-ui/core";
import {errorNotification, infoNotification, initAccounts, loginUser, logoutUser} from "../../redux/actions";
import {getCookieByName, postData, setCookie} from "../../tools/config";
import _ from "lodash";
import AppBar from "@material-ui/core/AppBar";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from '@material-ui/icons/AccountCircleRounded';
import {config} from "../../tools/config";
import Account from "../Account";
import AccessMiddleware from "../AccessMiddleware";
import {Route, Switch} from "react-router";
import AccountStatement from "../AccountStatement";
import AccountTransaction from "../AccountTransaction";
import {authenticateUser} from "../../tools/auth";

const styles = () => ({
    wrapper: {
        width: "100vw",
        height: "100vh",
        backgroundColor: '#fff'
    },
    header: {
        width: "100%",
        height: "50px"
    },
    main: {
        width: "100%",
        background: '#fff'
    },
    homeButton: {
        cursor: "pointer"
    }
});

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {},
            open: false,
            ancholEl: "",
            accounts: null
        };
        // check notifications =>
        let isCurrentAccChanged = false;
        if (props.socket) {
            console.log("socket -> ", props.socket);
            props.socket.on("data_change", async _not => {
                console.log("data changed ::", _not);
                const not_data = JSON.parse(_not);
                console.log("data parsed ::", not_data);
                if (not_data.key === "VIEW_SHAREHOLDER_ACCOUNTS") {
                    console.log("not_data.key => ", not_data.key);
                    if (this.state.accounts)
                        console.log("this.state.accounts => ", this.state.accounts);
                    this.state.accounts.forEach(account => {
                        console.log("not_data.data => ", not_data.data);
                        console.log("account => ", account);
                        if (not_data.data.filter(_acc => _acc === account.accountNumber).length > 0) {
                            isCurrentAccChanged = true;
                        }
                    });
                }
                console.log("isCurrentAccChanged => ", isCurrentAccChanged);
                if (isCurrentAccChanged) {
                    this.getAccounts()
                        .then(() => this.props.dispatch(infoNotification({text: "Məlumatlar yeniləndi."})))
                        .catch(e => this.props.dispatch(errorNotification({text: e.message})));
                }
            });
        }
    }

    //    data changed to::  {
//    "key": "VIEW_SHAREHOLDER_ACCOUNTS",
//    "data": [
//        "PER6939589143",
//        "PR6688839317",
//        "MN112946",
//        "DP00132083"
//        ],
//    "type": "AccountIdentificator"
//}


    toggleMenu = event => {
        this.setState({open: !this.state.open, anchorEl: event ? event.currentTarget : undefined});
    };

    logout = async () => {
        try {
            this.props.dispatch(logoutUser());
            const response = await fetch(config.logoutUrl);
            const body = await response.json();
            if (response.status !== 200) throw Error(body.message);
            return body;
        } catch (e) {
            console.log(e);
        }
    };

    handleMenu = async e => {
        this.toggleMenu(null);
        if (e.target.getAttribute("name") === "logout") {
            this.props.dispatch(logoutUser());
            await this.logout();
            setCookie(["token", ""], "/", "", {allowNull: true});
            this.props.history.push("/");
        }
    };

    getAccounts = async () => {
        const {user} = this.state;
        const {dispatch} = this.props;
        if (user)
            try {
                const credentials = {
                    token: user.token,
                    context: user.contexts[0]
                };
                const response = await postData({credentials}, config.getAccountURL);
                if (response && response.code === 0) {
                    dispatch(initAccounts(response.data.accounts));
                } else if (response && response.code === 2) {
                    this.props.dispatch(initAccounts([]));
                    this.props.dispatch(errorNotification({text: response.data.messageAz}));
                }
            } catch (e) {
                alert(e);
            }
    };

    componentDidMount = async () => {
        const {user} = this.state;
        const token = getCookieByName("token");
        if (_.isEmpty(token)) {
            this.props.history.push("/");
        }
        if (_.isEmpty(user)) {
            const resp = await authenticateUser(token);
            if (resp)
                if (resp.statusCode === 0) {
                    //TODO process context
                    this.props.dispatch(loginUser({
                        ...resp.responseData,
                        name: resp.responseData.person.nameAz,
                    }));
                } else if (resp.statusCode === 4) {
                    this.props.dispatch(errorNotification({text: resp.statusMessage}));
                    this.props.history.replace("/");
                } else if (resp.statusCode === 6) {
                    this.props.dispatch(errorNotification({text: resp.statusMessage}));
                } else {
                    this.props.dispatch(errorNotification({text: "Daxili xəta"}));
                }
        }
        // check notifications =>
//        this.props.socket.on("data_change", data => {
//            console.log("data changed to:: ", data);
//        });
    };

    componentDidUpdate = async (prevProps, prevState, snapshot) => {
        if (prevState.accounts === null && this.state.user)
            await this.getAccounts();
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.user !== prevState.user || nextProps.accounts !== prevState.accounts) {
            return {user: nextProps.user, accounts: nextProps.accounts};
        }
        return null
    }

    dispatchContext = async (token: string, ctx: object): Promise => {
        return new Promise((res, rej) => {
            if (!_.isEmpty(ctx)) {
                const user = {
                    status: "ASAN_IMZA",
                    token: token,
                    name: ctx.person.nameAz,
                    context: ctx
                };
                this.props.dispatch(loginUser(user));
                res(true);
            } else {
                rej("context is null!");
            }
        });
    };

    checkAuthentication = async token => new Promise(async (resolve, reject) => {
        try {
            const resp_data = await authenticateUser(token);
            if (resp_data.statusCode === 0) {
                await this.dispatchContext(token, resp_data.responseData);
                resolve(true);
            } else {
                this.props.dispatch(errorNotification({text: resp_data.statusMessage}));
                await this.props.dispatch(logoutUser());
                resolve(false);
            }
        } catch (e) {
            reject(e);
        }
    });


    render() {
        const {classes} = this.props;
        const {user, open, anchorEl} = this.state;
        return (
            <div className={`${classes.wrapper} d-flex flex-column justify-content-center align-items-center`}>
                <div className={`${classes.header} d-flex`}>
                    <AppBar position={"relative"} className={classes.header}>
                        <div className="d-flex flex-row justify-content-between">
                            <div className={classes.homeButton}
                                 onClick={() => this.props.history.push(`/dashboard`)}>
                                <Typography className={`p-2`} variant={"h6"} color={"secondary"}>
                                    Milli Depozit Mərkəzi
                                </Typography>
                            </div>
                            <Typography className={`p-2`} variant={"h6"} color={"secondary"}>
                                Investorlar üçün elektron xidmətlər
                            </Typography>
                            <div
                                className="account-info d-flex flex-row justify-content-between align-items-center justify-content-center">
                                <div className="d-flex flex-column align-items-center mr-1">
                                    <Typography variant={"body1"} component={"p"} className={"mr-2"}
                                                color={"secondary"}>
                                        {user && user.name}
                                    </Typography>
                                </div>
                                <IconButton
                                    aria-owns={'material-appbar'}
                                    aria-haspopup="true"
                                    color="inherit"
                                    onClick={this.toggleMenu}
                                >
                                    <AccountCircle/>
                                </IconButton>
                                <Menu id="render-props-menu" anchorEl={anchorEl} open={open} onClose={this.toggleMenu}>
                                    <MenuItem name={"logout"} onClick={this.handleMenu}>Çıxış</MenuItem>
                                </Menu>
                            </div>
                        </div>
                    </AppBar>
                </div>
                <main className={`${classes.main} d-flex flex-fill justify-content-center align-items-center`}>
                    <Switch>
                        <Route path={`${this.props.match.url}/account_transaction/:accountNumber`}
                               render={props => (
                                   <AccessMiddleware viewCode={"VIEW_SHAREHOLDER_ACCOUNT_TRANSACTIONS"}>
                                       <AccountTransaction props={{...props}}/>
                                   </AccessMiddleware>)}/>
                        <Route path={`${this.props.match.url}/account_statement/:accountNumber`}
                               render={props => (
                                   <AccessMiddleware viewCode={"VIEW_ACCOUNT_STATEMENT"}>
                                       <AccountStatement props={{...props}}/>
                                   </AccessMiddleware>)}/>
                        <Route path={`${this.props.match.url}`} render={props => (
                            <AccessMiddleware viewCode={"VIEW_SHAREHOLDER_ACCOUNTS"}>
                                <Account props={{...props}}/>
                            </AccessMiddleware>)}/>
                    </Switch>
                </main>
            </div>
        )
            ;
    }
}

const mapStateToProps = ({user, notification, loader, accounts, socket}) => ({
    user,
    notification,
    loader,
    accounts,
    socket
});

export default connect(
    mapStateToProps,
)(withStyles(styles)(Dashboard));
