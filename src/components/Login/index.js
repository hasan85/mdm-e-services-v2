import React, {Component} from 'react';
import {connect} from 'react-redux';
import withStyles from "@material-ui/core/styles/withStyles";
import LoginForm from "./LoginForm";
const styles = theme => ({
    wrapper: {
        width: "100vw",
        height: "100vh"
    }
});

class Login extends Component {
    render() {
        const {classes} = this.props;
        return (
            <div className={`${classes.wrapper} d-flex justify-content-center align-items-center`}>
                <div className="d-flex flex-column justify-content-center align-items-center">
                    <LoginForm />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        notification: state.notification,
        loader: state.loader
    };
}

export default connect(
    mapStateToProps,
)(withStyles(styles)(Login));
