import React, {Component} from 'react';
import {connect} from 'react-redux';
import {faBookOpen} from "@fortawesome/free-solid-svg-icons";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";
import logo from "./assets/asanimza_logo_blue.png";
import MaskedInput from "../../../custom_components/MaskedInput/index";
import _ from "lodash";
import {
    errorNotification,
    initSocket,
    loginUser,
    logoutUser,
    setSocketId,
    successNotification
} from "../../../redux/actions";
import {withRouter} from "react-router";
import {getContext, getCookieByName, getTokenExpireDate, setCookie} from "../../../tools/config";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import Loader from "react-loader-spinner";
import {theme} from "../../../tools/theme";
import {config, demoUser} from "../../../tools/config";
import {authenticateUser, loginUser as lu} from "../../../tools/auth";
import socket from "../../../redux/reducers/socket";


const styles = theme => ({
    root: {
        width: '100%',
        padding: 0,
        backgroundColor: 'transparent',
    },
    loginForm: {
        width: "1000px",
        height: "450px",
        backgroundColor: "transparent",
        boxShadow: "none",
        zndex: 10,
    },
    left_side: {
        backgroundColor: theme.palette.primary.main,
        zIndex: 10,
    },
    right_side: {
        zIndex: 9,
        left: "-200px",
        transition: "all 2s ease",
        transform: "translateX(200px)",
    },
    input: {
        color: "black"
    },
    rightSide: {
        backgroundColor: theme.palette.primary.contrastText
    },
    labelStyle: {
        color: '#000'
    },
    inputStyle: {
        color: "#000"
    },
    textField: {},
    rs_logo: {
        width: "250px"
    },
    rs_textfield: {
        width: "250px"
    },
    loader_paper: {}
});

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: {status: false},
            phone: "",
            asanID: "",
            errors: {},
            socketID: "",
            verificationCode: '',
            isAsanSocketAlive: false,
            socket: null,
            socketId: "",
        };

        // Credentials for DEMO login
        this.DEMO_PHONE = "+994111111111";
        this.DEMO_ID = "111111";
    }


    checkAuthentication = async token => new Promise(async (resolve, reject) => {
        try {
            const resp_data = await authenticateUser(token);
            if (resp_data.statusCode === 0) {
                await this.dispatchContext(token, resp_data.responseData);
                resolve(true);
            } else {
                this.props.dispatch(errorNotification({text: resp_data.statusMessage}));
                await this.props.dispatch(logoutUser());
                resolve(false);
            }
        } catch (e) {
            reject(e);
        }
    });

    getAsanVerificationCode = async () => new Promise((res, rej) => {
        const {socket} = this.state;
        socket && socket.on("verification_code_receive", data =>
            res(data)
        );
    });

    componentDidMount = async () => {
        this.setState({loader: {status: true}});
        const token = getCookieByName("token");
        const {socket} = this.state;

        /**
         * FOR TEST
         * */
        socket && socket.on("testServer", data => {
            console.log("test data from server via websocket -> ", data);
        });
        /**
         * END
         * */
        if (!_.isEmpty(token)) {
            try {
                const isAuthenticated = await this.checkAuthentication(token);
                if (isAuthenticated) {
                    this.setState({loader: {status: false}});
                    this.props.history.push("/dashboard");
                } else {
                    this.setState({loader: {status: false}});
                }
            } catch (e) {
                this.setState({loader: {status: false}});
                console.error("e -> ", e);
            }
        } else {
            this.setState({loader: {status: false}})
        }
        try {
            socket && socket.on("connection", client => {
            });
        } catch (e) {
            console.error(e);
        }
        try {
            const verificationCode = await this.getAsanVerificationCode();
            this.setState({verificationCode});
            console.log(verificationCode);
        } catch (e) {
            console.log(e);
            this.props.dispatch(errorNotification({text: e.message}));
        }

        await this.checkAsanSocketIsAlive();
    };

    checkAsanSocketIsAlive = async () => {
        const {isAsanSocketAlive, verificationCode} = this.state;
        console.log("isAsanSocketAlive -> ", isAsanSocketAlive);
        console.log("verificationCode -> ", verificationCode);
        if (!isAsanSocketAlive && _.isEmpty(verificationCode)) {
            console.log(" ** socket is alive");
            this.setState({isAsanSocketAlive: true});
            const verificationCode = await this.getAsanVerificationCode();
            this.setState({verificationCode, isAsanSocketAlive: false});
        }
    };

    componentDidUpdate = async (prevProps, prevState, snapshot) => {
        const {socket, socketId} = this.state;
        if (socket && socketId) {
            window.socket = socket;
//            alert("socket done");
            console.log(socketId)
        }
        await this.checkAsanSocketIsAlive();
    };

    sanitizeAsanNum = phone => {
        const {errors} = this.state;
        const _phone = phone.replace(/\s|\(|\)|_/g, "");
        console.log("phone => ", _phone.length)
        if (_phone && _phone.length < 4)
            return false;
        else if (_phone) {
            console.log("after return");
            if (typeof (_phone) !== "string" || _phone.length !== 13 || _phone.substring(0, 4) !== "+994") {
                errors["phone"] = "Format düzgün deyil!";
                this.setState({errors});
                return false;
            } else {
                console.log("else statement")
                errors["phone"] = "";
                this.setState({phone: _phone, errors});
                return true;
            }
        } else {
            errors["phone"] = "Telefon nömrənizi daxil edin.";
            this.setState({errors});
        }
    };

    sanitizeAsanID = asanID => {
        const {errors} = this.state;
        if (asanID) {
            if (asanID.length <= 6) {
                if (typeof (asanID) !== "string" || asanID.length !== 6) {
                    errors["asan_id"] = "Format düzgün deyil!";
                    this.setState({asanID, errors});
                    return false;
                } else {
                    this.setState({asanID, errors: {}});
                    return true;
                }
            }
        } else {
            errors["asan_id"] = "Asan id-nizi daxil edin";
            this.setState({errors, asanID});
        }

    };

    validate = () => {
        const {phone, asanID} = this.state;
        return !!(this.sanitizeAsanNum(phone) & this.sanitizeAsanID(asanID));
    };

    checkDemo = () => {
        const {phone, asanID} = this.state;
        return !!(phone === this.DEMO_PHONE && asanID === this.DEMO_ID);
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState.loader !== nextProps.loader || prevState.socket !== nextProps.socket || prevState.socketId !== nextProps.socketId) return {...nextProps};
        return null;
    }

    handleSocketTest = async () => {
        const resp = await this.checkSocket();
    };

    /**
     * method for testing socket
     * */
    checkSocket = async () => {
        const {socketID} = this.state;
        try {
            const response = await fetch("/test", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({socketID})
            });
            const body = await response.json();
            if (response.status !== 200) throw Error(body.message);
            return body;
        } catch (e) {
            console.log(e);
        }
    };

    dispatchContext = async (token: string, ctx: object): Promise => {
        return new Promise((res, rej) => {
            if (!_.isEmpty(ctx)) {
                const user = {
                    status: "ASAN_IMZA",
                    token: token,
                    name: ctx.person.nameAz,
                    ...ctx
                };
                this.props.dispatch(loginUser(user));
                res(true);
            } else {
                rej("context is null!");
            }
        });
    };

    handleSubmit = async e => {
        e.preventDefault();
        const {phone, asanID, socketId} = this.state;
        const isValid = this.validate();
        if (isValid) {
            this.setState({loader: {status: true}, verificationCode: ""});
            try {
                const loginResponse = await lu({phone, asanID, socketId});
                if (loginResponse) {
                    if (loginResponse.statusCode === 0) {
                        try {
                            const ctx_res = await this.dispatchContext(
                                loginResponse.responseData.token,
                                loginResponse.responseData.data
                            );
                            if (ctx_res) {
                                setCookie(
                                    ["token", loginResponse.responseData.token],
                                    "/",
                                    getTokenExpireDate(1),
                                    {allowNull: false}
                                );
                                this.setState({
                                    loader: {status: false},
                                    asanID: "",
                                    phone: ""
                                }, () => {
                                    this.props.dispatch(successNotification({text: "Siz müvəffəqiyyətlə sistemə daxil oldunuz"}));
                                    this.props.history.push("/dashboard");
                                });
                            } else {
                                this.props.dispatch(errorNotification("Daxili xəta"));
                            }
                        } catch (e) {
                            console.log(e);
                            this.props.dispatch(errorNotification("Daxili xəta"));
                        }
                    } else {
                        this.props.dispatch(errorNotification({text: loginResponse.data.messageAz}));
                        this.setState({
                            loader: {status: false},
                            asanID: "", phone: "",
                            verificationCode: ''
                        });
                    }
                } else {
                    this.props.dispatch(errorNotification({text: "Server error"}));
                    this.setState({loader: false})
                }
            } catch (ex) {
                console.log(ex);
                this.props.dispatch(errorNotification({text: ex.statusMessage}));
                this.setState({loader: {status: false}, verificationCode: ""})
            }

        }
    };

    render() {
        const {classes} = this.props;
        const {loader, errors, phone, asanID, verificationCode} = this.state;
        return (
            <div className={`${classes.loginForm} d-flex`}>
                <Paper
                    elevation={5}
                    className={`${classes.left_side} col-7 d-flex flex-column`}
                    square={true}
                >
                    <div className={`${classes.ls_top} col d-flex flex-column justify-content-center m-0 pr-0`}>
                        <Typography
                            variant="h4"
                            className={`${classes.ls_heading_1} m-0 p-0`}
                            color="secondary"
                            align="left"
                        >
                            Milli Depozit Mərkəzi
                        </Typography>
                        <Typography
                            variant="subtitle2"
                            className={`${classes.ls_service_name} mb-2 p-0`}
                            color="secondary"
                            align="left"
                        >
                            Elektron Xidmətlər
                        </Typography>
                        <Divider/>
                    </div>
                    <div className={`col ${classes.ls_bottom} d-flex flex-column justify-content-start m-0 pr-0`}>
                        <div className="d-flex flex-row justify-content-start align-items-baseline">
                            <FontAwesomeIcon
                                icon={faBookOpen}
                                color={"white"}
                                className="mt-2 mr-2"
                            />
                            <Typography color="secondary" variant="subtitle1">
                                İstifadəçi təlimatı
                            </Typography>
                        </div>
                    </div>
                </Paper>
                <Paper
                    className={`${classes.rightSide} col-5 d-flex justify-content-center align-items-center`}
                    square={true}
                    color={"secondary"}
                >
                    {loader.status ? (
                            <ReactCSSTransitionGroup
                                transitionName="loader"
                                transitionAppear={true}
                                transitionAppearTimeout={500}
                                transitionEnter={false}
                                transitionLeave={false}
                            >
                                <div
                                    className={
                                        `${classes.loader_paper} d-flex flex-column justify-content-center align-items-center`}>
                                    <Loader type="ThreeDots" color={theme.palette.primary.main} height="100" width="100"/>
                                    <Typography variant={"h6"} className={`text-center`}>
                                        Lütfən, telefonunuza göndərilmiş sorğunu qəbul edin. Sorğunun yoxlama kodunun
                                        aşağıdakı kod ilə eyni olmasını müqayisə edin.
                                    </Typography>
                                    {verificationCode ? (
                                        <Typography variant={"h5"} className={`text-center`} color={"primary"}>
                                            {`${verificationCode.verificationCode}`}
                                        </Typography>
                                    ) : <span/>}
                                </div>
                            </ReactCSSTransitionGroup>
                        )
                        : (
                            <form
                                className="col d-flex flex-column justify-content-center align-items-center"
                                onSubmit={this.handleSubmit}
                            >
                                <img className={`${classes.rs_logo} mb-5`} src={logo} alt="ASAN IMZA"/>
                                <MaskedInput
                                    label={errors ? errors["phone"] : "Telefon nömrəsi"}
                                    placeholder="Telefon nömrəsi"
                                    className={`${classes.textField} ${classes.rs_textfield} mb-4`}
                                    type="tel"
                                    color="black"
                                    onChange={e => this.sanitizeAsanNum(e.target.value)}
                                    error={errors && !_.isEmpty(errors["phone"])}
                                    inputlabelprops={{
                                        className: classes.labelStyle,
                                    }}
                                    value={phone}
                                    inputProps={{
                                        className: classes.inputStyle
                                    }}
                                />
                                <TextField
                                    className={`${classes.textField} ${classes.rs_textfield} mb-4`}
                                    type="password"
                                    label={errors ? errors["asan_id"] : "Asan ID"}
                                    placeholder={"Asan ID"}
                                    onChange={e => this.sanitizeAsanID(e.target.value)}
                                    value={asanID}
                                    error={errors && !_.isEmpty(errors["asan_id"])}
                                    inputlabelprops={{
                                        className: classes.labelStyle,
                                    }}
                                    inputProps={{
                                        className: classes.inputStyle,
                                        style: {'::WebkitInputPlaceholder': {color: "red"}}
                                    }}
                                />
                                <Button
                                    variant="contained"
                                    color="primary"
                                    type="submit"
                                    disableFocusRipple
                                    disabled={loader.status}
                                >
                                    Daxil ol
                                </Button>
                                {/*<Button onClick={this.handleSocketTest}>*/}
                                {/*    Socket Test*/}
                                {/*</Button>*/}
                            </form>
                        )}
                </Paper>
            </div>
        );
    }
}

//const mapStateToProps = ({socket}) => ({socket});
const mapStateToProps = (state) => {
    console.log("mapStateToProps => ", state);
    return {
        socket: state.socket,
        socketId: state.socketId
    }
};

export default withRouter(connect(
    mapStateToProps,
)(withStyles(styles)(LoginForm)));
