import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Route, Switch} from "react-router";
import {routes} from "./routes";

class Navigation extends Component {
    render() {
        return (
            <div>
                <Switch>
                    {
                        routes.map(route => {
                            return(
                                <Route key={route.path} exact={route.isExact} path={route.path}
                                       component={route.component}/>
                            )
                        })
                    }
                </Switch>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {};
}

export default connect(
    mapStateToProps,
)(Navigation);
