import Dashboard from "../Dashboard";
import Login from "../Login";

export const routes = [
    {
        path: "/dashboard/",
        component: Dashboard,
        isExact: false
    },
    {
        path: "/",
        component: Login,
        isExact: false
    },
];