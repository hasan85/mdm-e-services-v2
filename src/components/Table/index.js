import React, {Component} from 'react';
import {connect} from 'react-redux';
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";
import MaterialTable from "../../custom_components/MaterialTable";
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core";
import {blue} from "@material-ui/core/colors";
import {withRouter} from "react-router";

const styles = theme => {
    return ({
        wrapper: {
            width: "100vw",
            height: "100vh"
        }
    })
};

const rows = [
    {
        id: 'issuer',
        numeric: false,
        disablePadding: false,
        label: 'Emitent ',
        hasChildren: true,
        children: [
            {
                id: 'issuer_name',
                numeric: false,
                disablePadding: false,
                label: 'Adı',
                hasChildren: false
            },
            {
                id: 'issuer_adress',
                numeric: false,
                disablePadding: false,
                label: 'Hüquqi ünvanı',
                hasChildren: false
            }
        ]
    },
    {
        id: 'instrument',
        numeric: true,
        disablePadding: false,
        label: 'Qiymətli Kağız',
        hasChildren: true, children: [
            {
                id: 'instrument_code',
                numeric: true,
                disablePadding: false,
                label: 'ISIN nömrəsi',
                hasChildren: false
            },
            {
                id: 'cfi_name',
                numeric: true,
                disablePadding: false,
                label: 'Buraxılış forması və növü',
                hasChildren: false
            },
            {
                id: 'parValue',
                numeric: true,
                disablePadding: false,
                label: 'Nominal dəyəri',
                hasChildren: false
            },
            {
                id: 'instrument_quantity',
                numeric: true,
                disablePadding: false,
                label: 'Ümumi miqdar',
                hasChildren: false
            },
            {
                id: 'service_status',
                numeric: true,
                disablePadding: false,
                label: 'Xidmət statusu',
                hasChildren: false
            },
        ]
    },
    {
        id: 'balance_quantity',
        numeric: true,
        disablePadding: false,
        label: 'Mülkiyyətində olan kağızların sayı',
        hasChildren: false
    },
    {
        id: 'encumbrance_quantity',
        numeric: true,
        disablePadding: false,
        label: 'Öhdəliklərlə yüklənmiş',
        hasChildren: false
    }
];

const dark_theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: blue,
        secondary: {
            main: blue.A400
        }
    },
});

class Table extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [
                {
                    "issuer": {
                        "name": {
                            "id": 96264,
                            "nameAz": "Azəraqrartikinti ASC",
                            "nameEn": null,
                            "code": null
                        },
                        "address": {
                            "id": 219909,
                            "nameAz": "Bakı ş.,  Yasamal r-nu,, Şəfayət Mehdiyev küç., 93",
                            "nameEn": null,
                            "code": null
                        }
                    },
                    "balance": {
                        "balanceQuantity": 130,
                        "encumbranceQuantity": 0
                    },
                    "instrument": {
                        "cfiName": {
                            "id": 501,
                            "nameAz": "Adi Adlı Səhm",
                            "nameEn": "Common Stock",
                            "code": null
                        },
                        "instrumentCode": "AZ1001005557",
                        "parValue": 20,
                        "instrumentQuantity": 561470,
                        "serviceStatus": {
                            "id": null,
                            "nameAz": "XidmÉ™t olunur",
                            "nameEn": "Served",
                            "code": null
                        }
                    }
                },
                {
                    "issuer": {
                        "name": {
                            "id": 96264,
                            "nameAz": "Azəraqrartikinti ASC",
                            "nameEn": null,
                            "code": null
                        },
                        "address": {
                            "id": 219909,
                            "nameAz": "Bakı ş.,  Yasamal r-nu,, Şəfayət Mehdiyev küç., 93",
                            "nameEn": null,
                            "code": null
                        }
                    },
                    "balance": {
                        "balanceQuantity": 130,
                        "encumbranceQuantity": 0
                    },
                    "instrument": {
                        "cfiName": {
                            "id": 501,
                            "nameAz": "Adi Adlı Səhm",
                            "nameEn": "Common Stock",
                            "code": null
                        },
                        "instrumentCode": "AZ1001005557",
                        "parValue": 20,
                        "instrumentQuantity": 561470,
                        "serviceStatus": {
                            "id": null,
                            "nameAz": "XidmÉ™t olunur",
                            "nameEn": "Served",
                            "code": null
                        }
                    }
                },
                {
                    "issuer": {
                        "name": {
                            "id": 96264,
                            "nameAz": "Azəraqrartikinti ASC",
                            "nameEn": null,
                            "code": null
                        },
                        "address": {
                            "id": 219909,
                            "nameAz": "Bakı ş.,  Yasamal r-nu,, Şəfayət Mehdiyev küç., 93",
                            "nameEn": null,
                            "code": null
                        }
                    },
                    "balance": {
                        "balanceQuantity": 130,
                        "encumbranceQuantity": 0
                    },
                    "instrument": {
                        "cfiName": {
                            "id": 501,
                            "nameAz": "Adi Adlı Səhm",
                            "nameEn": "Common Stock",
                            "code": null
                        },
                        "instrumentCode": "AZ1001005557",
                        "parValue": 20,
                        "instrumentQuantity": 561470,
                        "serviceStatus": {
                            "id": null,
                            "nameAz": "XidmÉ™t olunur",
                            "nameEn": "Served",
                            "code": null
                        }
                    }
                }
            ]
        }
    }

    render() {
        return (
            <div className={`d-flex justify-content-center align-items-center`}>
                <div className="d-flex flex-column justify-content-center align-items-center">
                    <MuiThemeProvider theme={dark_theme}>
                        <MaterialTable rows={rows} data={this.state.data}/>
                    </MuiThemeProvider>
                    <Button
                        color={"primary"}
                        variant={"contained"}
                        onClick={() => this.props.history.goBack()}>Back</Button>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        notification: state.notification,
        loader: state.loader
    };
}

export default withRouter(connect(mapStateToProps)(withStyles(styles)(Table)));
