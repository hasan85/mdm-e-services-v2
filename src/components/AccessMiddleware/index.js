import React, {Component} from 'react';
import {connect} from 'react-redux';
import {config, postData} from "../../tools/config";
import {errorNotification, loginUser, logoutUser, updateUser} from "../../redux/actions"
import {ACCESS_GRANTED, NOT_ALLOWED, NONE} from "./StatusCodes";
import {withRouter} from "react-router";
import Loader from "react-loader-spinner";
import {authenticateUser, logoutUser as lo} from "../../tools/auth";
import _ from "lodash";

class AccessMiddleware extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {},
            isDataFetced: false,
            securityStatus: ACCESS_GRANTED
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState.user !== nextProps.user) {
            return {
                user: nextProps.user
            };
        }
        return null;
    }

    dispatchContext = async (token: string, ctx: object): Promise => {
        return new Promise((res, rej) => {
            if (!_.isEmpty(ctx)) {
                const user = {
                    status: "ASAN_IMZA",
                    token: token,
                    name: ctx.person.nameAz,
                    context: ctx
                };
                this.props.dispatch(loginUser(user));
                res(true);
            } else {
                rej("context is null!");
            }
        });
    };

    checkAuthentication = async token => new Promise(async (resolve, reject) => {
        try {
            const resp_data = await authenticateUser(token);
            if (resp_data.statusCode === 0) {
                await this.dispatchContext(token, resp_data.responseData);
                resolve(true);
            } else {
                this.props.dispatch(errorNotification({text: resp_data.statusMessage}));
                await this.props.dispatch(logoutUser());
                resolve(false);
            }
        } catch (e) {
            reject(e);
        }
    });


    componentDidUpdate = async (prevProps, prevState, snapshot) => {
        const {dispatch, viewCode} = this.props;
        const {isDataFetced, user, securityStatus} = this.state;
        if (securityStatus === NOT_ALLOWED) {
            this.props.dispatch(errorNotification({text: NOT_ALLOWED}))
        }
        if (user) {
            if (!isDataFetced) {
                this.setState({isDataFetced: true});
                const ctx = user.contexts[0];
                try {
                    const resp = await postData({
                        credentials: {
                            token: user.token,
                            data: ctx
                        }
                    }, config.getUserParametersURL);
                    if (resp) {
                        if (resp.code !== 0) {
                            this.props.dispatch(errorNotification({text: resp.messageAz}));
                            await lo();
                            this.props.history.replace("/")
                        }
                        if (resp.code === 0) {
                            const user_parameters = JSON.parse(resp.user_parameters);
                            if (user_parameters.views.filter(view => view.code === viewCode).length > 0) {
                                dispatch(updateUser(user_parameters));
                                this.setState({securityStatus: ACCESS_GRANTED});
                            } else {
                                this.setState({securityStatus: NOT_ALLOWED});
                            }
                        }
                    }
                } catch (e) {
                    console.log(e);
                }
            } else {
//            this.props.history.push("/");
            }
        }
    };

    componentDidMount = async () => {
        const {dispatch, viewCode} = this.props;
        const {isDataFetced, user, securityStatus} = this.state;
        if (securityStatus === NOT_ALLOWED) {
            alert(NOT_ALLOWED)
        }
        if (user) {
            if (!isDataFetced) {
                this.setState({isDataFetced: true});
                const ctx = user.contexts[0];
                try {
                    const resp = await postData({
                        credentials: {
                            token: user.token,
                            data: ctx
                        }
                    }, config.getUserParametersURL);
                    if (resp) {
                        if (resp.code !== 0) {
                            this.props.dispatch(errorNotification({text: resp.messageAz}))
                        }
                        if (resp.code === 0) {
                            const user_parameters = JSON.parse(resp.user_parameters);

                            if (user_parameters.views.filter(view => view.code === viewCode).length > 0) {
                                dispatch(updateUser(user_parameters));
                                this.setState({securityStatus: ACCESS_GRANTED});
                            } else {
                                this.setState({securityStatus: NOT_ALLOWED});
                            }
                        } else {
                        }
                    }
                } catch (e) {
                    console.log(e);
                }
            }
        } else {
            console.log("user doesn't exist!");
        }
    };

    render() {
        const {securityStatus} = this.state;
        return (
            <div className={'d-flex flex-fill justify-content-center align-items-center'} style={{height: "100%"}}>
                {securityStatus === NONE
                    ? (<div>
                        <Loader type="ThreeDots"/>
                    </div>)
                    : (securityStatus === ACCESS_GRANTED ? this.props.children : <div>{NOT_ALLOWED}</div>)
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

export default withRouter(connect(
    mapStateToProps,
)(AccessMiddleware));
