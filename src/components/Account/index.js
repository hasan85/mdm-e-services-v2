import React, {Component} from 'react';
import {connect} from 'react-redux';
import withStyles from "@material-ui/core/styles/withStyles";
import {Typography, Tooltip} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import RemoveRedEye from "@material-ui/icons/RemoveRedEyeRounded";
import AccountBalanceWallet from "@material-ui/icons/AccountBalanceWalletRounded";
import {withRouter} from "react-router";

const styles = theme => ({
    wrapper: {
        width: "100vw",
        height: "100vh"
    },
    dashboard_tile: {
        backgroundColor: theme.palette.primary.main,
        width: "250px",
        height: "200px",
        padding: '20px'
    },
    dashboardContainer: {
        width: "800px"
    },
    textContainer: {},
    content: {},
    labelText: {
        color: "#DFD9D985",
    },
    buttonsContainer: {}

});

class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            accounts: []
        }
    }

    componentDidMount = async () => {
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.user !== prevState.user || nextProps.accounts !== prevState.accounts)
            return {
                user: nextProps.user,
                accounts: nextProps.accounts
            };
        return null
    }


    render() {
        const {classes} = this.props;
        const {accounts} = this.state;
        return (
            <div className={`d-flex justify-content-center align-items-center flex-fill`}>
                <div className="d-flex flex-column justify-content-center align-items-center flex-fill">
                    <div className={`${classes.content} d-flex justify-content-center align-items-center flex-fill`}>
                        <div
                            className={`${classes.dashboardContainer} d-flex flex-row flex-wrap justify-content-center `}>
                            {accounts && accounts.map((account, index) => (
                                <Paper
                                    className={
                                        `${classes.dashboard_tile}
                                        d-flex 
                                        flex-column 
                                        justify-content-center 
                                        align-items-center 
                                        m-2`
                                    }
                                    key={index}>
                                    <div className={`${classes.textContainer}`}>
                                    <span>
                                        <Typography
                                            className={classes.labelText}
                                            variant={"subtitle2"}
                                            color={"textSecondary"}
                                        >
                                            Hesabın nömrəsi
                                        </Typography>
                                        <Typography variant={"h6"}
                                                    color={"secondary"}>{account.accountNumber}</Typography>
                                    </span>
                                        <span>
                                        <Typography
                                            className={classes.labelText}
                                            variant={"subtitle2"}
                                            color={"textSecondary"}
                                        >
                                            Hesabın operatoru
                                        </Typography>
                                        <Typography
                                            variant={"h6"}
                                            color={"secondary"}
                                        >
                                            {account.operator && account.operator.nameAz}
                                        </Typography>
                                    </span>
                                    </div>
                                    <div className={`${classes.buttonsContainer} d-flex flex-row p-2`}>
                                   <span>
                                       <Tooltip title={"Kağızlara bax"}>
                                           <IconButton
                                               aria-owns={'material-appbar'}
                                               aria-haspopup="true"
                                               color="inherit"
                                               onClick={() => this.props.history.push(
                                                   `${this.props.match.url}/account_statement/${account.accountNumber}`
                                               )}
                                           >
                                           <RemoveRedEye color={"secondary"}/>
                                       </IconButton>
                                       </Tooltip>
                                   </span>
                                    <span>
                                        <Tooltip title={"Əməliyyatlara bax"}>
                                            <IconButton
                                               aria-owns={'material-appbar'}
                                               aria-haspopup="true"
                                               color="inherit"
                                               onClick={() => this.props.history.push(
                                                   `${this.props.match.url}/account_transaction/${account.accountNumber}`
                                               )}>
                                               <AccountBalanceWallet color={"secondary"}/>
                                            </IconButton>
                                       </Tooltip>
                                    </span>
                                    </div>
                                </Paper>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        notification: state.notification,
        loader: state.loader,
        accounts: state.accounts
    };
}

export default withRouter(connect(mapStateToProps)(withStyles(styles)(Account)));