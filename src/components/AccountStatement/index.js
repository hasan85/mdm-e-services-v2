import React, {Component} from 'react';
import {connect} from 'react-redux';
import {AgGridReact} from 'ag-grid-react/main';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import 'ag-grid-community/dist/styles/ag-theme-dark.css';
import 'ag-grid-community/dist/styles/ag-theme-blue.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import 'ag-grid-community/dist/styles/ag-theme-bootstrap.css';
import 'ag-grid-community/dist/styles/ag-theme-balham-dark.css';
import _ from "lodash";
import {withRouter} from "react-router";
import {config, postData, textFilterParams, numberFilterParams, dateFilterParams} from "../../tools/config";
import {errorNotification} from "../../redux/actions";
import withStyles from "@material-ui/core/styles/withStyles";
import ToolTip from "@material-ui/core/Tooltip";
import Fab from "@material-ui/core/Fab";
import CloseIcon from "@material-ui/icons/CloseRounded"
import ExportIcon from "@material-ui/icons/CloudDownloadRounded"
import ResetIcon from "@material-ui/icons/ClearAllRounded";
import Typography from "@material-ui/core/Typography";


class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            columnDefs: [
                {
                    headerName: "Emitent",
                    headerTooltip: "Emitent",
                    suppressPadding: true,
                    children: [
                        {
                            field: "issuer_name",
                            headerName: "Adı",
                            headerTooltip: "Adı",
                            width: 400,
                            sortable: true,
                            tooltipField: "issuer_name",
                            filter: "agTextColumnFilter",
                            filterParams: textFilterParams
                        },
                        {
                            field: "issuer_adress",
                            headerName: "Hüquqi ünvanı",
                            headerTooltip: "Hüquqi ünvanı",
                            width: 350,
                            sortable: true,
                            tooltipField: "issuer_adress",
                            filter: "agTextColumnFilter",
                            filterParams: textFilterParams,
                        }
                    ]
                },
                {
                    headerName: "Qiymətli kağız",
                    headerTooltip: "Qiymətli kağız",
                    children: [
                        {
                            headerName: "ISIN nömrəsi",
                            headerTooltip: "ISIN nömrəsi",
                            field: "instrument_code",
                            tooltipField: "instrument_code",
                            width: 120,
                            sortable: true,
                            filter: "agTextColumnFilter",
                            filterParams: textFilterParams,
                        },
                        {
                            headerName: "Buraxılış forması və növü",
                            headerTooltip: "Buraxılış forması və növü",
                            field: "cfi_name",
                            tooltipField: "cfi_name",
                            width: 150,
                            sortable: true,
                            filter: "agTextColumnFilter",
                            filterParams: textFilterParams,
                        },
                        {
                            headerName: "Nominal dəyəri",
                            headerTooltip: "Nominal dəyəri",
                            field: "par_value",
                            tooltipField: "par_value",
                            width: 120,
                            sortable: true,
                            filter: "agNumberColumnFilter",
                            filterParams: numberFilterParams,
                        },
                        {
                            headerName: "Valyuta",
                            headerTooltip: "Valyuta",
                            field: "instrument_currency",
                            tooltipField: "instrument_currency",
                            width: 100,
                            sortable: true,
                            filter: "agTextColumnFilter",
                            filterParams: textFilterParams,
                        },
                        {
                            headerName: "Ümumi miqdar",
                            headerTooltip: "Ümumi miqdar",
                            field: "instrument_quantity",
                            tooltipField: "instrument_quantity",
                            width: 120,
                            sortable: true,
                            filter: "agNumberColumnFilter",
                            filterParams: numberFilterParams,
                        },
                        {
                            key: "service_status",
                            headerName: `Xidmət statusu 🛈`,
                            headerTooltip: "Xidmət statusu",
                            field: "service_status",
                            tooltipField: "service_status",
                            width: 150,
                            sortable: true,
                            filter: "agTextColumnFilter",
                            filterParams: textFilterParams,
                        }
                    ]
                },
                {
                    headerName: "Mülkiyyətində olan kağızların sayı",
                    headerTooltip: "Mülkiyyətində olan kağızların sayı",
                    width: 250,
                    children: [{
                        headerName: "",
                        headerTooltip: "Mülkiyyətində olan kağızların sayı",
                        field: "balance_quantity",
                        tooltipField: "balance_quantity",
                        sortable: true,
                        filter: "agNumberColumnFilter",
                        filterParams: numberFilterParams,
                    }]
                },
                {
                    headerName: "Öhdəliklərlə yüklənmiş",
                    headerTooltip: "Öhdəliklərlə yüklənmiş",
                    width: 250,
                    children: [{
                        headerName: "",
                        headerTooltip: "Öhdəliklərlə yüklənmiş",
                        field: "encumbrance_quantity",
                        tooltipField: "balance_quantity",
                        sortable: true,
                        filter: "agNumberColumnFilter",
                        filterParams: numberFilterParams,
                    }]
                }
            ],
            defaultColDef: {resizable: true},
            rowData: null,
            accounts: [],
            uawe: {},
            isDataFetched: false,
            currentAccount: null
        };
        this.requestDone = false;
    }

    sanitizeData = data => {
        if (data && data.length > 0) {
            const _data = data.map(row => {
                if (_.isEmpty(row.balance) || _.isEmpty(row.instrument) || _.isEmpty(row.issuer)) {
                    this.props.dispatch(errorNotification({
                        text: "Incoming data incorrect", callback: () => {
                        }
                    }));
                    return null;
                }
                const raw = {};
                raw.issuer_name = row.issuer.name.nameAz;
                raw.issuer_adress = row.issuer.address.nameAz;
                raw.instrument_code = row.instrument.instrumentCode;
                raw.instrument_currency = row.instrument.currency.code;
                raw.cfi_name = row.instrument.cfiName.nameAz;
                raw.par_value = row.instrument.parValue;
                raw.instrument_quantity = row.instrument.instrumentQuantity;
                raw.service_status = row.instrument.serviceStatus.nameAz;
                raw.balance_quantity = row.balance.balanceQuantity;
                raw.encumbrance_quantity = row.balance.encumbranceQuantity;
                return raw
            });
            return _data;
        }
    };

    getAccountStatements = async () => new Promise(async resolve => {
        const {accounts, rowData, user} = this.state;
        const {match} = this.props;
        if (_.isEmpty(rowData) && accounts) {
            const accountNumber = match.params.accountNumber;
            const currentAccountList = accounts.filter(account => account.accountNumber === accountNumber);
            const currentAccount = currentAccountList && currentAccountList.length > 0 ? currentAccountList[0] : null;
            if (currentAccount && user) {
                const credentials = {
                    token: user.token,
                    context: user.contexts[0],
                    account: currentAccount
                };
                const resp = await postData(
                    {credentials},
                    config.getAccountStatementURL
                );
                if (!_.isEmpty(resp)) {
                    if (resp.code === 0) {
                        const rowData = this.sanitizeData(resp.data.accountStatements);
                        this.setState({rowData, currentAccount: resp.data.account}, () => {
                            resolve(true);
                        });
                    } else {
                        this.setState({rowData: []});
                        this.props.dispatch(errorNotification({text: resp.data.messageAz}));
                    }
                }
            }
        }
    });

    componentDidMount = async () => {
        const isDataFetched = await this.getAccountStatements();
        this.setState({isDataFetched: !!isDataFetched});
    };

    componentDidUpdate = async (prevProps, prevState, snapshot) => {
        let isRowsSet = false;
        if (_.isEmpty(prevState.rowData) && this.state.accounts && !this.requestDone) {
            this.requestDone = true;
            isRowsSet = await this.getAccountStatements();
        }
        if (!this.state.isDataFetched && isRowsSet) {
            this.setState({isDataFetched: true}, () => {
                this.gridApi.sizeColumnsToFit();
            });

        }
    };

    static getDerivedStateFromProps = (nextProps, prevState) => {
        if (nextProps.accounts !== prevState.accounts || nextProps.user !== prevState.user) {
            const {accounts, user} = nextProps;
            return {accounts, user};
        }
        return null;
    };

    onExport = () => {
        const params = {
            skipHeader: false,
            columnGroups: true,
            skipFooters: true,
            skipGroups: true,
            skipPinnedTop: true,
            skipPinnedBottom: true,
            allColumns: true,
            onlySelected: false,
            suppressQuotes: false,
            fileName: "Data Table",
            columnSeparator: ''
        };
        this.gridApi.exportDataAsCsv(params);
    };

    onGridReady = x => {
        this.gridApi = x.api;
        this.gridColumnApi = x.columnApi;
        x.columnApi.autoSizeColumns();
    };

    clearFilters = () => {
        this.gridApi.setFilterModel(null);
        this.gridApi.onFilterChanged();
    };

    render() {
        const {rowData, columnDefs, isDataFetched, currentAccount} = this.state;
        const {classes} = this.props;
        return (
            <div className="d-flex flex-fill" style={{height: '100%'}}>
                <div className={`flex-fill flex-column justify-content-between`}>
                    <div className={`d-flex flex-row justify-content-between ${classes.tableToolbar}`}>
                        <div className={`d-flex flex-grow-1 flex-column justify-content-start pl-3 pt-2`}>
                            <Typography variant={"h5"} color={"secondary"}>
                                Depo hesabındakı kağızlar barədə məlumat
                            </Typography>
                            <div className={`d-flex flex-row`}>
                                <Typography variant={"subtitle1"} color={"secondary"}>
                                    {currentAccount && currentAccount.accountNumber}
                                </Typography>
                                <Typography variant={"subtitle1"} color={"secondary"} className={`ml-3`}>
                                    {
                                        currentAccount &&
                                        currentAccount.operator &&
                                        currentAccount.operator.nameAz
                                    }
                                </Typography>
                                <Typography
                                    variant={"subtitle1"}
                                    color={"secondary"}
                                    className={`ml-5 pl-5`}>
                                    {new Date().toLocaleDateString()}
                                </Typography>
                            </div>

                        </div>
                        <div className={`d-flex flex-row-reverse justify-content-start`}>
                            <ToolTip title={"Bağla"} disableHoverListener={!isDataFetched}>
                                <div>
                                    <Fab
                                        size={"small"}
                                        variant={"round"}
                                        color={"primary"}
                                        className={`m-3 ${classes.fab}`}
                                        onClick={() => this.props.history.push("/dashboard")}
                                        aria-label={"Daha çox"}
                                        disabled={!isDataFetched}
                                    >
                                        <CloseIcon fontSize={"small"}/>
                                    </Fab>
                                </div>
                            </ToolTip>
                            <ToolTip title={"Excel export"}>
                                <div>
                                    <Fab
                                        size={"small"}
                                        variant={"round"}
                                        color={"primary"}
                                        className={`m-3 ${classes.fab}`}
                                        disabled={!isDataFetched}
                                        onClick={() => {
                                            this.onExport()
                                        }}
                                        aria-label={"Export"}
                                    >
                                        <ExportIcon fontSize={"small"}/>
                                    </Fab>
                                </div>
                            </ToolTip>
                            <ToolTip title={"Filterləri sıfırla"}>
                                <div>
                                    <Fab
                                        size={"small"}
                                        variant={"round"}
                                        color={"primary"}
                                        className={`m-3 ${classes.fab}`}
                                        disabled={!isDataFetched}
                                        onClick={() => {
                                            this.clearFilters()
                                        }}
                                        aria-label={"Clear filters"}
                                    >
                                        <ResetIcon fontSize={"small"} color={"secondary"}/>
                                    </Fab>
                                </div>
                            </ToolTip>
                        </div>
                    </div>
                    <div className={`ag-theme-blue ${classes.statements}`}>
                        <AgGridReact
                            enableColResize={true}
                            headerHeight={40}
                            rowHeight={40}
                            onGridReady={this.onGridReady}
                            enableSorting={true}
                            pagination={true}
                            paginationPageSize={18}
                            columnDefs={columnDefs}
                            rowData={rowData}
                            enableCellTextSelection={true}
                            enableBrowserTooltips={true}
                            suppressExcelExport={false}
                        >
                        </AgGridReact>
                    </div>
                    {/*<div style={{width: '100%', height: '40px'}} className="ag-theme-balham">*/}
                    {/*    <AgGridReact*/}
                    {/*        onGridReady={this.onGridReady}*/}
                    {/*        columnDefs={[*/}
                    {/*            {*/}
                    {/*                headerName: 'Məlumat',*/}
                    {/*                field: 'info',*/}
                    {/*                width: '500'*/}
                    {/*            },*/}
                    {/*        ]}*/}
                    {/*        rowData={[*/}
                    {/*            {*/}
                    {/*                info: "Bu siyahıda göstərilən əməliyyatlar fevral 2016-cı ildən sonrakı dövrü əhatə edir."*/}
                    {/*            }*/}
                    {/*        ]}*/}
                    {/*        gridOptions={{alignedGrids: []}}*/}
                    {/*        headerHeight={0}*/}
                    {/*        rowStyle={{fontWeight: 'bold'}}*/}
                    {/*    >*/}
                    {/*    </AgGridReact>*/}
                    {/*</div>*/}
                </div>
            </div>
        );
    }
}

const styles = theme => {
    return {
        statements: {
            width: "100%",
            height: "85%",
            backgroundColor: '#cccccc'
        },
        fab: {
            backgroundColor: theme.palette.primary.main,
            color: "white"
        },
        tableToolbar: {
            marginTop: "5px",
            backgroundColor: theme.palette.tableColor.main
        }
    }
};

const mapStateToProps = state => {
    return {
        accounts: state.accounts,
        user: state.user
    };
};

export default withRouter(withStyles(styles)(connect(
    mapStateToProps,
)(Index)));
