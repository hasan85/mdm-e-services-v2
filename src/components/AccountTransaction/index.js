import React, {Component} from 'react';
import {connect} from 'react-redux';
import {AgGridReact} from 'ag-grid-react/main';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import 'ag-grid-community/dist/styles/ag-theme-dark.css';
import 'ag-grid-community/dist/styles/ag-theme-balham-dark.css';
import _ from "lodash";
import {withRouter} from "react-router";
import {config, postData, textFilterParams, numberFilterParams, dateFilterParams} from "../../tools/config";
import {errorNotification} from "../../redux/actions";
import withStyles from "@material-ui/core/styles/withStyles";
import ToolTip from "@material-ui/core/Tooltip/Tooltip";
import Fab from "@material-ui/core/Fab";
import CloseIcon from "@material-ui/icons/CloseRounded";
import ExportIcon from "@material-ui/icons/CloudDownloadRounded"
import ResetIcon from "@material-ui/icons/ClearAllRounded";
import Typography from "@material-ui/core/Typography";


class AccountTransaction extends Component {

    constructor(props) {
        super(props);
        this.state = {
            columnDefs: [
                {
                    headerName: "QK məlumatları",
                    headerTooltip: "QK məlumatları",
                    children: [
                        {
                            field: "issuer_data",
                            headerName: "Investor məlumatları",
                            headerTooltip: "Investor məlumatları",
                            tooltipField: "issuer_data",
                            width: 350,
                            autoHeight: true,
                            cellStyle: () => ({
                                height: '90px',
                                whiteSpace: 'pre-line',
                                fontWeight: 500
                            }),
                            resizable: true,
                            sortable: true,
                            filter: "agTextColumnFilter",
                            filterParams: textFilterParams,
                        }
                    ]
                },
                {
                    headerName: "Əqd məlumatları",
                    headerTooltip: "Əqd məlumatları",
                    rowGroup: true,
                    children: [
                        {
                            headerName: "Hesablaşma tarixi",
                            headerTooltip: "Hesablaşma tarixi",
                            tooltipField: "settlement_date",
                            field: "settlement_date",
                            width: 120,
                            sortable: true,
                            filter: "agDateColumnFilter",
                            filterParams: dateFilterParams,
                            cellStyle: () => ({
                                fontWeight: 500
                            })
                        },
                        {
                            headerName: "Növü",
                            headerTooltip: "Növü",
                            tooltipField: "settlement_type",
                            field: "settlement_type",
                            width: 150,
                            sortable: true,
                            filter: "agTextColumnFilter",
                            filterParams: textFilterParams,
                            cellStyle: () => ({
                                fontWeight: 500
                            })

                        },
                        {
                            headerName: "Nömrəsi",
                            tooltipField: "settlement_number",
                            headerTooltip: "Hesablaşmanın nömrəsi",
                            field: "settlement_number",
                            width: 230,
                            sortable: true,
                            cellStyle: () => ({
                                wordBreak: "break-word",
                                whiteSpace: "pre-wrap",
                                lineHeight: 'normal',
                                fontWeight: 500
                            }),
                            filter: "agTextColumnFilter",
                            filterParams: textFilterParams,
                        },
                        {
                            headerName: "Debet",
                            headerTooltip: "Debet",
                            tooltipField: "settlement_debet",
                            field: "settlement_debet",
                            width: 100,
                            sortable: true,
                            filter: "agNumberColumnFilter",
                            filterParams: numberFilterParams,
                            cellStyle: () => ({
                                color: "#ff0303e0",
                                fontWeight: 700
                            })
                        },
                        {
                            headerName: "Kredit",
                            headerTooltip: "Kredit",
                            tooltipField: "settlement_kredit",
                            field: "settlement_kredit",
                            width: 120,
                            sortable: true,
                            filter: "agNumberColumnFilter",
                            filterParams: numberFilterParams,
                            cellStyle: () => ({
                                color: "#409f48",
                                fontWeight: 700
                            })
                        }
                    ]
                }
            ],
            defaultColDef: {resizable: true, filter: true},
            rowData: null,
            accounts: [],
            uawe: {},
            isDataFetched: false,
            currentAccount: null
        };
        this.requestDone= false;
    }

    sanitizeData = data => {
        if (data && data.length > 0) {
            const _data = data.map(row => {
                if (_.isEmpty(row.issuer) || _.isEmpty(row.instrument) || _.isEmpty(row.settlement)) {
                    this.props.dispatch(errorNotification({text: "Incoming data incorrect"}));
                    return null;
                }
                const raw = {};
                raw.issuer_data = `${row.instrument.instrumentCode},\n${row.issuer.nameAz},\n${row.instrument.parValue} ${row.instrument.currency.code}`;
                raw.settlement_date = row.settlement.date.split(" ")[0];
                raw.settlement_type = row.settlement.type.nameAz;
                raw.settlement_number = row.settlement.number;
                raw.settlement_debet = row.settlement.debet === 0 ? 0 : row.settlement.debet === null ? "" : row.settlement.debet;
                raw.settlement_kredit = row.settlement.kredit === 0 ? 0 : row.settlement.kredit === null ? "" : row.settlement.kredit;
                return raw
            });
            return _data;
        } else {
            return [];
        }
    };

    getAccountTransactions = async () => new Promise(async (resolve, reject) => {
        const {accounts, rowData, user} = this.state;
        const {match} = this.props;
        if (_.isEmpty(rowData) && accounts) {
            const accountNumber = match.params.accountNumber;
            const currentAccountList = accounts.filter(account => account.accountNumber === accountNumber);
            const currentAccount = currentAccountList && currentAccountList.length > 0 ? currentAccountList[0] : null;
            if (currentAccount && user) {
                const credentials = {
                    token: user.token,
                    context: user.contexts[0],
                    data: {account: currentAccount}
                };
                const resp = await postData(
                    {credentials},
                    config.getAccountTransactionURL
                );
                if (resp && resp.code === 0) {
                    const rowData = this.sanitizeData(resp.data.accountTransactions);
                    this.setState({rowData, currentAccount: resp.data.account}, () => {
                        resolve(true);
                    });
                } else {
                    this.setState({rowData: []}, () => resolve(false));
                    reject(resp.data);
                    this.props.dispatch(errorNotification({text: resp.data.messageAz}));
                }
            }
        }
    });

    componentDidMount = async () => {
        const isDataFetched = await this.getAccountTransactions();
        this.setState({isDataFetched: !!isDataFetched});
    };

    componentDidUpdate = async (prevProps, prevState, snapshot) => {
        let isRowsSet = false;
        if (_.isEmpty(prevState.rowData) && this.state.accounts && !this.requestDone) {
            this.requestDone = true;
            isRowsSet = await this.getAccountTransactions();
        }
        if (!this.state.isDataFetched && isRowsSet) {
            this.setState({isDataFetched: true}, () => {
                this.gridApi.sizeColumnsToFit();
            });

        }
    };

    static getDerivedStateFromProps = (nextProps, prevState) => {
        if (nextProps.accounts !== prevState.accounts || nextProps.user !== prevState.user) {
            const {accounts, user} = nextProps;
            return {accounts, user};
        }
        return null;
    };

    onExport() {
        const params = {
            skipHeader: false,
            columnGroups: true,
            skipFooters: true,
            skipGroups: true,
            skipPinnedTop: true,
            skipPinnedBottom: true,
            allColumns: true,
            onlySelected: false,
            suppressQuotes: false,
            fileName: "Data Table",
            columnSeparator: ''
        };
        this.gridApi.exportDataAsCsv(params);
    }

    onGridReady = x => {
        this.gridApi = x.api;
        this.gridColumnApi = x.columnApi;
    };

    clearFilters = () => {
        this.gridApi.setFilterModel(null);
        this.gridApi.onFilterChanged();
    };

    render() {
        const {rowData, columnDefs, isDataFetched, currentAccount} = this.state;
        const {classes} = this.props;
        return (
            <div className="d-flex flex-fill" style={{height: 'calc(100% - 20px)'}}>
                <div className={`flex-fill flex-column justify-content-between`}>
                    <div className={`d-flex flex-row justify-content-between ${classes.tableToolbar}`}>
                        <div className={`d-flex flex-grow-1 flex-column justify-content-start pl-3 pt-2`}>
                            <Typography variant={"h5"} color={"secondary"}>
                                Depo hesabındakı əməliyyatlar barədə məlumat
                            </Typography>
                            <div className={`d-flex flex-row`}>
                                <Typography variant={"subtitle1"} color={"secondary"}>
                                    {currentAccount && currentAccount.accountNumber}
                                </Typography>
                                <Typography variant={"subtitle1"} color={"secondary"} className={`ml-3`}>
                                    {
                                        currentAccount &&
                                        currentAccount.operator &&
                                        currentAccount.operator.nameAz
                                    }
                                </Typography>
                                <Typography
                                    variant={"subtitle1"}
                                    color={"secondary"}
                                    className={`ml-5 pl-5`}>
                                    {new Date().toLocaleDateString()}
                                </Typography>
                            </div>

                        </div>
                        <div className={`d-flex flex-row-reverse justify-content-start`}>
                            <ToolTip title={"Bağla"} disableHoverListener={!isDataFetched}>
                                <div>
                                    <Fab
                                        size={"small"}
                                        variant={"round"}
                                        color={"primary"}
                                        className={`m-3 ${classes.fab}`}
                                        onClick={() => this.props.history.push("/dashboard")}
                                        aria-label={"Daha çox"}
                                        disabled={!isDataFetched}
                                    >
                                        <CloseIcon fontSize={"small"}/>
                                    </Fab>
                                </div>
                            </ToolTip>
                            <ToolTip title={"Excel export"}>
                                <div>
                                    <Fab
                                        size={"small"}
                                        variant={"round"}
                                        color={"primary"}
                                        className={`m-3 ${classes.fab}`}
                                        disabled={!isDataFetched}
                                        onClick={() => {
                                            this.onExport()
                                        }}
                                        aria-label={"Export"}
                                    >
                                        <ExportIcon fontSize={"small"}/>
                                    </Fab>
                                </div>
                            </ToolTip>
                            <ToolTip title={"Filterləri sıfırla"}>
                                <div>
                                    <Fab
                                        size={"small"}
                                        variant={"round"}
                                        color={"primary"}
                                        className={`m-3 ${classes.fab}`}
                                        disabled={!isDataFetched}
                                        onClick={() => {
                                            this.clearFilters()
                                        }}
                                        aria-label={"Clear filters"}
                                    >
                                        <ResetIcon fontSize={"small"} color={"secondary"}/>
                                    </Fab>
                                </div>
                            </ToolTip>
                        </div>
                    </div>
                    <div className={`ag-theme-blue ${classes.statements}`}>
                        <AgGridReact
                            enableColResize={true}
                            headerHeight={40}
                            rowHeight={40}
                            onGridReady={this.onGridReady}
                            enableSorting={true}
                            pagination={true}
                            paginationPageSize={18}
                            columnDefs={columnDefs}
                            rowData={rowData}
                            enableCellTextSelection={true}
                            enableBrowserTooltips={true}
                            suppressAndOrCondition={false}
                            defaultColDef={{
                                filter: true
                            }}
                        />
                    </div>
                    <div style={{width: '100%', height: '40px'}} className="ag-theme-balham">
                        <AgGridReact
                            onGridReady={this.onGridReady}
                            columnDefs={[
                                {
                                    headerName: 'Məlumat',
                                    field: 'info',
                                    width: '500'
                                },
                            ]}
                            colWidth={'auto'}
                            rowData={[
                                {
                                    info: "Bu siyahıda göstərilən əməliyyatlar fevral 2016-cı ildən sonrakı dövrü əhatə edir."
                                }
                            ]}
                            gridOptions={{alignedGrids: []}}
                            headerHeight={0}
                            rowStyle={{fontWeight: 'bold'}}
                        >
                        </AgGridReact>
                    </div>
                </div>
            </div>
        );
    }
}

const styles = theme => ({
    statements: {
        width: "100%",
        height: "85%",
        backgroundColor: '#cccccc'
    },
    fab: {
        backgroundColor: theme.palette.primary.main,
        color: "white"
    },
    tableToolbar: {
        marginTop: "5px",
        backgroundColor: theme.palette.tableColor.main
    }
});

const mapStateToProps = state => {
    return {
        accounts: state.accounts,
        user: state.user
    };
};

export default withRouter(withStyles(styles)(connect(
    mapStateToProps,
)(AccountTransaction)));
