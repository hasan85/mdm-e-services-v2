import React, {Component} from 'react';
import {connect} from 'react-redux';
import {AgGridReact} from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import _ from "lodash";

const sanitizeData = data => {
    if (data && data.length > 0) {
        const _data = data.map(row => {
            if (_.isEmpty(row.balance) || _.isEmpty(row.instrument) || _.isEmpty(row.issuer)) {
                alert("Incoming data incorrect");
                //this.props.dispatch(errorNotification({ text: "Incoming data incorrect", callback: () => {}}));
                return null;
            }
            const raw = {};
            raw.issuer_name = row.issuer.name.nameAz;
            raw.issuer_adress = row.issuer.address.nameAz;
            raw.instrument_code = row.instrument.instrumentCode;
            raw.cfi_name = row.instrument.cfiName.nameAz;
            raw.par_value = row.instrument.parValue;
            raw.instrument_quantity = row.instrument.instrumentQuantity;
            raw.service_status = row.instrument.serviceStatus.nameAz;
            raw.balance_quantity = row.balance.balanceQuantity;
            raw.encumbrance_quantity = row.balance.encumbranceQuantity;
            return raw
        });
        return _data;
    }
};

class DevExpressTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            columnDefs: [
                {
                    headerName: "Emitent",
                    children: [
                        {
                            field: "issuer_name",
                            headerName: "Adı",
                            width: 120
                        },
                        {
                            field: "issuer_adress",
                            headerName: "Hüquqi ünvanı",
                            width: 200
                        }
                    ]
                },
                {
                    headerName: "Qiymətli kağız",
                    children: [
                        {
                            headerName: "ISIN nömrəsi",
                            field: "instrument_code",
                            width: 120
                        },
                        {
                            headerName: "Buraxılış forması və növü",
                            field: "cfi_name",
                            width: 150
                        },
                        {
                            headerName: "Nominal dəyəri",
                            field: "par_value",
                            width: 120
                        },
                        {
                            headerName: "Ümumi miqdar",
                            field: "instrument_quantity",
                            width: 120
                        },
                        {
                            headerName: "Xidmət statusu",
                            field: "service_status",
                            width: 140
                        }
                    ]
                },
                {
                    headerName: "Mülkiyyətində olan kağızların sayı",
                    children: [{
                        headerName: "",
                        field: "balance_quantity"
                    }]
                },
                {
                    headerName: "Öhdəliklərlə yüklənmiş",
                    children: [{
                        headerName: "",
                        field: "encumbrance_quantity"
                    }]
                }
            ],
            defaultColDef: {resizable: true},
            rowData: []
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.rowData !== prevState.rowData) {
            const rowData = sanitizeData(nextProps.rowData);
            return {rowData};
        }
        return null;
    }


    render() {
        const {rowData, columnDefs} = this.state;
        return (
            <div
                className="ag-theme-balham"
                style={{
                    height: '90vh',
                    width: '100vw'
                }}
            >
                <AgGridReact
                    columnDefs={columnDefs}
                    rowData={rowData}>
                </AgGridReact>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(
    mapStateToProps,
)(DevExpressTable);
