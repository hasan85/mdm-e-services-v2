import React from "react";
import InputMask from "react-input-mask";
import MaterialInput from "@material-ui/core/Input";
import _ from "lodash";

const beforeChange = (newState, oldState, userInput) => {
    const newPhone = newState.value.replace(/\s|\(|\)|_/g, "");
    const oldPhone = oldState.value.replace(/\s|\(|\)|_/g, "");

    if (newPhone === oldPhone)
        return newState;
    else {
        if (_.isEmpty(userInput)) {
//            debugger;
            if (oldPhone === "+994") {
                return {...oldState, value: "(+994)"}
            } else if (oldState.selection.start <= 5 || oldState.selection.end <= 5)
                return oldState;
        } else return newState;
    }
};

const MaskedInput = props => (
    <InputMask
        label={props.label}
        defaultValue="+994"
        mask="(+999) 99 999 99 99"
        onChange={props.onChange}
        className={props.className}
        error={props.error}
        permanents={[0, 4]}
        beforeMaskedValueChange={beforeChange}
    >
        {inputProps => <MaterialInput
            inputlabelprops={props.inputLabelProps}
            inputProps={props.inputProps}
            className={props.className}
            label="blabla"
            type="tel"
            {...inputProps}/>}
    </InputMask>
);

export default MaskedInput;

//export default class MaskedInput extends React.Component {
//    state = {
//        value: ''
//    };
//
//    onChange = (event) => {
//        this.setState({
//            value: event.target.value
//        });
//    };
//
//    beforeMaskedValueChange = (newState, oldState, userInput) => {
//        let {value} = newState;
//        let selection = newState.selection;
//        let cursorPosition = selection ? selection.start : null;
//
//        // keep minus if entered by user
//        if (value.endsWith('-') && userInput !== '-' && !this.state.value.endsWith('-')) {
//            if (cursorPosition === value.length) {
//                cursorPosition--;
//                selection = {start: cursorPosition, end: cursorPosition};
//            }
//            value = value.slice(0, -1);
//        }
//
//        return {
//            value,
//            selection
//        };
//    };
//
//    render() {
//        return <InputMask
//            mask="(+999) 99 999 99 99"
//            maskChar={null}
//            value={this.state.value}
//            onChange={this.onChange}
//            beforeMaskedValueChange={this.beforeMaskedValueChange}>
//            {inputProps => <MaterialInput
//                inputlabelprops={this.props.inputLabelProps}
//                inputProps={this.props.inputProps}
//                className={this.props.className}
//                label="blabla"
//                type="tel"
//                {...inputProps}/>}
//        </InputMask>;
//    }
//}