import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Icon from '@mdi/react';
import {mdiFileExport} from '@mdi/js';
import FilterListIcon from '@material-ui/icons/FilterList';
import {connect} from "react-redux";
import _ from "lodash";

const desc = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
};

const stableSort = (array, cmp) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
};

const getSorting = (order, orderBy) => {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
};

const AccountTableHeadStyles = theme => ({
    motherCell: {}
});

let AccountTableHead = props => {


    const createSortHandler = property => event => {
        props.onRequestSort(event, property);
    };

    const {rows, onSelectAllClick, order, orderBy, numSelected, rowCount, classes} = props;
    return (
        <TableHead>
            <TableRow>
                <TableCell padding="checkbox">
                    <Checkbox
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={numSelected === rowCount}
                        onChange={onSelectAllClick}
                    />
                </TableCell>
                {rows.map(
                    row => (
                        <TableCell
                            key={row.id}
                            align={'left'}
                            padding={row.disablePadding ? 'none' : 'default'}
                            sortDirection={orderBy === row.id ? order : false}
                        >
                            <TableRow>
                                {row.hasChildren
                                    ? (
                                        <TableRow component={"tr"}>
                                            <TableCell key={row.id}
                                                       component={"td"}
                                                       align={'left'}
                                                       className={`d-flex justify-content-center`}
                                                       padding={row.disablePadding ? 'none' : 'default'}
                                                       sortDirection={orderBy === row.id ? order : false}>
                                                {row.label}
                                            </TableCell>
                                            <TableRow component={"tr"} className={`${classes.motherCell} d-flex flex-row`}>
                                                {row.children.map((childRow) => (
                                                    <Tooltip
                                                        title="Sort"
                                                        placement={
                                                            childRow.numeric
                                                                ? 'bottom-end'
                                                                : 'bottom-start'
                                                        }
                                                        enterDelay={300}
                                                    >
                                                        <TableSortLabel
                                                            active={orderBy === childRow.id}
                                                            direction={order}
                                                            onClick={createSortHandler(childRow.id)}
                                                            className={`d-flex flex-row `}
                                                        >
                                                            <TableCell className={`mr-2`} component={"td"}>
                                                                {childRow.label}
                                                            </TableCell>
                                                        </TableSortLabel>
                                                    </Tooltip>
                                                ))}
                                            </TableRow>
                                        </TableRow>
                                    )
                                    : (
                                        <TableRow className={`d-flex flex-row`}>
                                            <Tooltip
                                                title="Sort"
                                                placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                                                enterDelay={300}
                                            >
                                                <TableSortLabel
                                                    active={orderBy === row.id}
                                                    direction={order}
                                                    onClick={createSortHandler(row.id)}
                                                    className={`d-flex flex-row justify-content-center`}
                                                >
                                                    <TableCell>{row.label}</TableCell>
                                                </TableSortLabel>
                                            </Tooltip>
                                        </TableRow>
                                    )
                                }
                            </TableRow>
                        </TableCell>
                    ),
                    this,
                )}
            </TableRow>
        </TableHead>
    )
};

AccountTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

AccountTableHead = withStyles(AccountTableHeadStyles)(AccountTableHead);

const toolbarStyles = theme => ({
    root: {
        paddingRight: theme.spacing.unit,
    },
    highlight: {},
    spacer: {
        flex: '1 1 100%',
    },
    actions: {
        color: theme.palette.common.white,
    },
    title: {
        color: theme.palette.common.white,
        flex: '0 0 auto',
    },
});

let TableToolbar = props => {
    const {numSelected, classes} = props;

    return (
        <Toolbar
            className={classNames(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
            <div className={classes.title}>
                {numSelected > 0 ? (
                    <Typography color="inherit" variant="subtitle1">
                        {numSelected} selected
                    </Typography>
                ) : (
                    <Typography variant="h6" id="tableTitle">
                        Depo hesabından elektron çıxarış
                    </Typography>
                )}
            </div>
            <div className={classes.spacer}/>
            <div className={classes.actions}>
                {numSelected > 0 ? (
                    <Tooltip title="Export">
                        <IconButton aria-label="Export" onClick={() => this.exportCSV}>
                            <Icon path={mdiFileExport}
                                  size={1}
                                  color="green"/>
                        </IconButton>
                    </Tooltip>
                ) : (
                    <Tooltip title="Filter list">
                        <IconButton aria-label="Filter list">
                            <FilterListIcon/>
                        </IconButton>
                    </Tooltip>
                )}
            </div>
        </Toolbar>
    );
};

TableToolbar.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
};

TableToolbar = withStyles(toolbarStyles)(TableToolbar);

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    table: {
        width: "1000px",
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    row: {
        width: '200px',
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.grey["600"],
        },
    },
});

const sanitizeData = data => {
    if (data && data.length > 0) {
        const _data = data.map(row => {
            if (_.isEmpty(row.balance) || _.isEmpty(row.instrument) || _.isEmpty(row.issuer)) {
                alert("Incoming data incorrect");
                //this.props.dispatch(errorNotification({ text: "Incoming data incorrect", callback: () => {}}));
                return null;
            }
            const raw = {};
            raw.issuer_name = row.issuer.name.nameAz;
            raw.issuer_adress = row.issuer.address.nameAz;
            raw.instrument_code = row.instrument.instrumentCode;
            raw.cfi_name = row.instrument.cfiName.nameAz;
            raw.par_value = row.instrument.parValue;
            raw.instrument_quantity = row.instrument.instrumentQuantity;
            raw.service_status = row.instrument.serviceStatus.nameAz;
            raw.balance_quantity = row.balance.balanceQuantity;
            raw.encumbrance_quantity = row.balance.encumbranceQuantity;
            return raw
        });
        return _data;
    }
};

class MaterialTable extends React.Component {
    state = {
        order: 'asc',
        orderBy: 'calories',
        selected: [],
        data: {},
        page: 0,
        rowsPerPage: 5,
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState.data === nextProps.data) return null;
        else {
            const data = sanitizeData(nextProps.data);
            return {data};
        }
    }

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order, orderBy});
    };

    handleSelectAllClick = event => {
        if (event.target.checked) {
            this.setState(state => ({selected: state.data.forEach(n => n.id)}));
            return;
        }
        this.setState({selected: []});
    };

    handleClick = (event, id) => {
        const {selected} = this.state;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        this.setState({selected: newSelected});
    };

    handleChangePage = (event, page) => {
        this.setState({page});
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };

    isSelected = id => this.state.selected.indexOf(id) !== -1;

    render() {
        const {classes, rows} = this.props;
        const {data, order, orderBy, selected, rowsPerPage, page} = this.state;

        return (
            <Paper className={classes.root}>
                <TableToolbar numSelected={selected.length} classes={{}}/>
                <div className={classes.tableWrapper}>
                    <Table className={classes.table} aria-labelledby="tableTitle">
                        <AccountTableHead
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={this.handleSelectAllClick}
                            onRequestSort={this.handleRequestSort}
                            rowCount={data.length}
                            rows={rows}
                        />
                        <TableBody>
                            {stableSort(data, getSorting(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map(n => {
                                    const isSelected = this.isSelected(n.id);
                                    return (
                                        <TableRow
                                            hover
                                            onClick={event => this.handleClick(event, n.id)}
                                            onDoubleClick={() => {
                                                console.log("row n -> ", n);
                                            }}
                                            role="checkbox"
                                            aria-checked={isSelected}
                                            tabIndex={-1}
                                            key={n.id}
                                            selected={isSelected}
                                            className={classes.row}
                                        >
                                            <TableCell padding="checkbox">
                                                <Checkbox checked={isSelected}/>
                                            </TableCell>
                                            {Object.keys(n).map((value, index) => {
                                                return value !== "id" && (
                                                    <TableCell
                                                        key={index}
                                                        component="th"
                                                        scope="row"
                                                        padding="none"
                                                        align="left">
                                                        {/*<TableCell>{n[value]}</TableCell>*/}
                                                        {/*<TableCell>{n[value]}</TableCell>*/}
                                                        {n[value]}
                                                    </TableCell>
                                                );
                                            })}

                                        </TableRow>
                                    );
                                })}
                        </TableBody>
                    </Table>
                </div>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 15]}
                    component="div"
                    count={data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                        'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'Next Page',
                    }}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
            </Paper>
        );
    }
}

MaterialTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mstp = state => ({});

export default connect(mstp)(withStyles(styles)(MaterialTable));
