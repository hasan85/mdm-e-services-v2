import React, {Component} from 'react';
import './App.css';
import {withRouter} from "react-router";
import {connect} from "react-redux";
import Navigation from "./components/Navigation";
import {ToastContainer, toast} from 'react-toastify';
import {parseJwt} from "./tools/config";
import {hideNotification, initSocket, setSocketId} from "./redux/actions";
import SocketIoClient from "socket.io-client";

class App extends Component {

    constructor(props) {
        super(props);
        window.parseJwt = parseJwt;
        this.state = {
            notifications: [],
            notification: null
        };
        const socket = SocketIoClient();
        props.dispatch(initSocket(socket));
        socket.connect();
        socket.on('connect', () => {
            const socketId = socket.id;
            console.log("socket id -> ", socket.id);
            this.props.dispatch(setSocketId(socketId));
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {notification, notifications} = this.state;
        const self = this;
        if (notifications.length === 0 && notification.status) {
            notifications.push(notification);
            this.setState({notifications}, () => {
                this.notify(notification.type, notification.text, () => {
                    this.props.dispatch(hideNotification());
                    if (notification.callback)
                        notification.callback();
                        self.setState({notifications: []});
                });
            });
        } else {
            console.log("another notification is showing: ", notification);
        }
    }

    componentDidMount = () => {

    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.notification !== prevState.notification)
            return {notification: nextProps.notification};
        return null;
    }


    notify = (type, text, callback) => toast[type](text, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        onClose: props => callback && callback(props)
    });

    render() {
        return (
            <div>
                <Navigation/>
                <ToastContainer/>
            </div>
        );
    }
}

const mapStateToProps = ({notification}) => ({
    notification
});

export default withRouter(connect(mapStateToProps)(App));
