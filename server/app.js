const app = require("express")();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const config = require("./config").config;
require("node-schedule");
require("./express")(app);


io.on("connection", client => {
    console.log("client id => ", client.id);
    require("./routes")(app, io);
    require("./socketAsanImza")(io);
});

http.listen(config.port, () =>
    console.log(`Server started at the port: ${config.port}`)
);
