const path = require("path");
const atob = require('atob');

console.log("root -> ", path.resolve(__dirname, "../"));
exports.config = {
    root: path.resolve(__dirname, "../"),
    port: process.env.PORT || 8080,
    ip: "192.168.31.14",
    thriftClientPort: 55557,
    thriftEServicePort: 55558,
    thriftServerPort: 9095,
    secrets: {
        session: "qaw9d8qwdas9udas-0d9asjadja90hasdasdh"
    }
};

exports.parseJwt = token => {
    const base64Url = token.split('.')[1];
    const base64 = base64Url && base64Url.replace(/-/g, '+').replace(/_/g, '/');
    if (!base64) return null;
    return JSON.parse(atob(base64));
};