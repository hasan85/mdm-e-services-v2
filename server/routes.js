const parseJwt = require("./config").parseJwt;
const config = require("./config").config;
const path = require("path");
const _ = require("lodash");
const thriftWrapper = require("./thrift/thriftWrapper");
const Request = require("./thrift/gen-nodejs/auth_types").AuthRequest;
const AuthService = require("./thrift/gen-nodejs/authService");
const esService = require("./thrift/gen-nodejs/esService");
const EsTypes = require("./thrift/gen-nodejs/es_types");
const thrift = require("thrift");


module.exports = (app, io) => {

    app.get("/test_notification", (req, res) => {
        io.sockets.emit("data_change", {status: "done", data: "test"});
        res.json({
            status: "blabla",
            client_id: "testost"
        });
    });

    app.post("/test", (req, res) => {
        const socket_id = req.body.socketID;
        res.json({
            status: "blabla",
            client_id: socket_id
        });
        io.sockets.connected[socket_id].emit("testServer", {status: "done", clientID: socket_id})
    });

    app.post("/set_context", (req, res) => {
        const token = req.body.token;
        if (token) {
            req.session.user_context = {token};
            res.json({code: 0, context: req.session.user_context});
        } else {
            res.json({code: 1, message: "No token exception"});
        }
    });

    app.post("/get_context", (req, res) => {
        const token = req.body.token;
        const session = req.session;
        if (!token || typeof (token) !== "string") {
            res.json({
                code: 2,
                message: "Token is null or not string!"
            });
        } else {
            const ctx = session.user_context ? JSON.parse(session.user_context) : null;
            if (isTokenExpired(token)) {
                res.json({
                    code: 4,
                    message: "Sessiya başa çatıb zəhmət olmasa yenidən daxil olun."
                })
            }
            if (ctx && _.isEqual(ctx.token, token)) {
                const d = {...ctx, code: 0};
                res.json(d);
            } else {
                req.session.user_context = null;
                req.session.user_parameters = null;
                res.json({
                    code: 2,
                    message: "User not signed in!"
                })
            }
        }
    });

    app.get("/logout", async (req, res) => {
        try {
            req.session.user_context = null;
            res.json({
                code: 0
            });
        } catch (e) {
            res.json({
                code: 2,
                message: e.message
            })
        }
    });

    app.post("/asan_imza_login", async (req, res) => {
        const phone = req.body.phone;
        const asanID = req.body.asanID;
        const socketID = req.body.socketId;

        const args = {
            isAsanLogin: true,
            phone,
            asanID,
            roles: ["ROLE_SHAREHOLDER"]
        };

        const con = thriftWrapper.createConnection(
            config.ip,
            config.thriftClientPort
        );
        const th_client = thriftWrapper.createLoginUIClient(con, AuthService);
        const request = new Request({
            code: 1,
            token: "notoken",
            socketID,
            data: JSON.stringify(args)
        });
        th_client
            .authServiceRequest(request)
            .then(data => {
                if (data.code === 0) {
                    const context = data.data;
                    const user = {
                        token: data.token,
                        data: JSON.parse(context)
                    };
                    req.session.user_context = JSON.stringify(user);
                    res.json({code: data.code, user});
                    console.log("user authenticated!", user);
                } else {
                    // TODO handle other codes -->
                    console.error("error -> ", data);
                    res.json({
                        code: data.code,
                        data: JSON.parse(data.data)
                    });
                }
            })
            .catch(ex => console.log(ex));

        con.on("error", function (err) {
            console.error("error -> ", err);
        });
    });

    app.post("/get_user_parameters", async (req, res) => {
        const credentials = req.body.credentials;
        const con = thriftWrapper.createConnection(
            config.ip,
            config.thriftClientPort
        );
        const th_client = thriftWrapper.createLoginUIClient(con, AuthService);
        const request = new Request({
            code: 2,
            token: credentials.token,
            socketID: "noSocket",
            data: JSON.stringify(credentials.data)
        });
        th_client
            .authServiceRequest(request)
            .then(data => {
                if (data.code === 0) {
                    const context = data.data;
                    req.session.user_parameters = JSON.stringify(context);
                    res.json({
                        code: data.code,
                        user_parameters: context
                    });
                    console.log("user authenticated!", context);
                } else {
                    // TODO handle other codes -->
                    console.error(data);
                    const respDdata = JSON.parse(data.data)
                    res.json(respDdata);
                }
            })
            .catch(ex => console.log(ex));

        con.on("error", function (err) {
            console.error("error -> ", err);
        });
    });

    app.post("/get_account", async (req, res) => {
        const credentials = req.body.credentials || {};
        const ctx = credentials.context;
        const token = credentials.token;
        if (_.isEmpty(credentials) || _.isEmpty(ctx) || _.isEmpty(token)) {
            res.json({
                code: 2,
                message: "Data is incomplete!"
            });
        }
        const connection = thriftWrapper.createConnection(config.ip, config.thriftEServicePort);
        const ThClient = thriftWrapper.createLoginUIClient(connection, esService);

        const request = new EsTypes.ESRequest({
            code: 1,
            token: token,
            context: JSON.stringify(ctx),
            data: ""
        });
        ThClient
            .esServiceRequest(request)
            .then(data => {
                console.log("response data -> ", data);
                res.json({code: data.code, data: JSON.parse(data.data)});
            })
            .catch(ex => console.log(ex));

        connection.on("error", function (err) {
            console.error("error -> ", err);
        });
    });

    app.post("/get_account_statement", async (req, res) => {
        const credentials = req.body.credentials || {};
        const ctx = credentials.context;
        const token = credentials.token;
        const account = credentials.account;
        if (_.isEmpty(credentials) || _.isEmpty(ctx) || _.isEmpty(token) || _.isEmpty(account)) {
            res.json({
                code: 2,
                message: "Data is incomplete!"
            });
        }
        const connection = thriftWrapper.createConnection(config.ip, config.thriftEServicePort);
        const ThClient = thriftWrapper.createLoginUIClient(connection, esService);
        const request = new EsTypes.ESRequest({
            code: 2,
            token: token,
            context: JSON.stringify(ctx),
            data: JSON.stringify({account})
        });
        ThClient
            .esServiceRequest(request)
            .then(data => {
                console.log("response data -> ", data);
                res.json({code: data.code, data: JSON.parse(data.data)});
            })
            .catch(ex => console.log(ex));

        connection.on("error", function (err) {
            console.error("error -> ", err);
        });
    });

    app.post("/get_account_transaction", async (req, res) => {
        const credentials = req.body.credentials || {};
        const ctx = credentials.context;
        const token = credentials.token;
        const data = credentials.data;
        if (_.isEmpty(credentials) || _.isEmpty(ctx) || _.isEmpty(token) || _.isEmpty(data)) {
            res.json({
                code: 2,
                message: "Data is incomplete!"
            });
        }
        const connection = thriftWrapper.createConnection(config.ip, config.thriftEServicePort);
        const ThClient = thriftWrapper.createLoginUIClient(connection, esService);
        const request = new EsTypes.ESRequest({
            code: 3,
            token: token,
            context: JSON.stringify(ctx),
            data: JSON.stringify(data)
        });
        ThClient
            .esServiceRequest(request)
            .then(data => {
                console.log("response data -> ", data);
                res.json({code: data.code, data: JSON.parse(data.data)});
            })
            .catch(ex => console.log(ex));

        connection.on("error", function (err) {
            console.error("error -> ", err);
        });
    });

    app.get("/*", (req, res) => {
        res.sendFile(path.join(config.root, "build", "index.html"));
    });

    /* *********************************
    ********** Checks token expires date
    ********************************* */
    const isTokenExpired = token => {
        if (token) {
            const tokenData = parseJwt(token);
            if (!tokenData)
                return true;
            const exp_date = new Date(tokenData.exp * 1000);
            const now = new Date();
            return now > exp_date;
        } else
            return true;
    };

};