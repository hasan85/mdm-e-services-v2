﻿/*
 *
 * CSDR sistemi
 *
 *  Məlumat bazası ilə (Oracle) E-Services (CSDR DB) arasında məlumat mübadiləsi
 *
 * versiya 1
 * 3 Aprel 2019
 *
 */

namespace java az.ndc.commons.thrift
 
typedef i32 int
 
struct DBRequest			                // Sorğu
{
  1: required int code,		        		// Sorğunun kodu (bax - 'Service Request Codes.xlsx' faylı)
  2: string data		          	        // Sorğu məlumatı
}

struct DBResponse		                    // Sorğuya cavab
{
  1: required int code,		        		// Sorğuya cavab kodu (0 - OK, <>0 - Səhv)
  2: required string data		            // Sorğuya cavab məlumatı
}

service DBService
{
  /****************************************************************************************
   *
       DB Service
   *
  ******************************************************************************************/
  
    DBResponse dbServiceRequest(1: DBRequest request)
}

