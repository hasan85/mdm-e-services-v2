const thrift = require("thrift"),
    WebService = require("./gen-nodejs/webService"),
    transport = require("../../node_modules/thrift/lib/nodejs/lib/thrift/framed_transport"),
    protocol = require("../../node_modules/thrift/lib/nodejs/lib/thrift/binary_protocol"),
    _ = require("lodash");

module.exports = async (port, io) => {
    const server = thrift.createServer(
        WebService,
        {
            webServiceRequest: response => webServiceRequest(response, io)
        },
        {transport: transport, protocol: protocol}
    );

    return new Promise((resolve, reject) => {
        server.listen(port, () => {
            console.log(" <- Thrift server started -> ");
            return resolve(true);
        });
    });
};

const webServiceRequest = (response, io) => {
    if (response.code === 1) {
        const {data, socketID} = response;
        const desiredSocket = io.sockets.connected[socketID];
        if (!_.isEmpty(desiredSocket)) {

            desiredSocket.emit(
                "verification_code_receive",
                {
                    verificationCode: data
                }
            );
        }
    } else if (response.code === 2) {
        console.log("response -> ", response);
        io.sockets.emit("data_change", response.data);
    } else {
        console.log(`response -> ${response}`);
    }
};
