﻿/*
 *
 * CSDR System
 *
 *  Data flow from Presentation Server (NodeJS) to Application Server (CSDR BE)
 *
 * version 1
 * 25 January 2019
 *
 */

namespace java az.mdm.thrift
 
typedef i32 integer

struct UIRequest				            // Request
{
	1: required integer code,		    	// Request Code (reference file - 'Service Codes.xlsx')
	2: required string token,  	    		// User token 
	3: required string data		          	// Request Data
}

struct UIResponse				            // Response
{
	1: required integer code,		    	// Response Code (reference file - 'Service Codes.xlsx')
	2: required string data		          	// Response Data
}



service UIService
{
  /****************************************************************************************
   *
        Data Exchange Service
   *
  ******************************************************************************************/
  
    UIResponse uiServiceRequest(1: UIRequest request)
}

