/**
 * Thrift wrapper ->
 */
const thrift = require("thrift"),
  transport = require("../../node_modules/thrift/lib/nodejs/lib/thrift/framed_transport"),
  protocol = require("../../node_modules/thrift/lib/nodejs/lib/thrift/binary_protocol");

const createConnection = (ip, port) =>
  thrift.createConnection(ip, port, {
    transport:transport,
    protocol: protocol
  });

const createLoginUIClient = (connection, service) => thrift.createClient(service, connection);
const closeLoginUIClient = connection => connection.end();

/**============================== */
/** <- Thrift http connection end */
exports.createConnection = createConnection;
exports.createLoginUIClient = createLoginUIClient;
exports.closeLoginUIClient = closeLoginUIClient;
