﻿/*
 *
 * CSDR System
 *
 *  Data exchange between Presentation Server (NodeJS) and Authentication Server (CSDR AUTH)
 *
 * version 1
 * 25 January 2019
 *
 */

namespace java az.ndc.commons.thrift
 
typedef i32 integer

struct AuthRequest			                // Request
{
	1: required integer code,		    	// Request Code (reference file - 'Service Codes.xlsx')
	2: required string token,  	    		// User token ("notoken", if absent)
	3: required string socketID,			// User socket ID 
	4: required string data		          	// Request Data
}

struct AuthResponse				            // Response
{
	1: required integer code,		    	// Response Code (reference file - 'Service Codes.xlsx')
	2: required string token,  	    		// User token ("notoken", if absent)
	3: required string socketID,			// User socket ID 
	4: required string data		          	// Response Data
}



service AuthService
{
  /****************************************************************************************
   *
        Auth Service
   *
  ******************************************************************************************/
  
    AuthResponse authServiceRequest(1: AuthRequest request)
}

