﻿/*
 *
 * CSDR sistemi
 *
 *  Istifadəçi interfeysi ilə (NodeJS) E-Services (CSDR ES) arasında məlumat mübadiləsi
 *
 * versiya 1
 * 27 mart 2019
 *
 */

namespace java az.ndc.commons.thrift
 
typedef i32 int
 
struct ESRequest			                // Sorğu
{
  1: required int code,		        		// Sorğunun kodu (bax - 'Service Request Codes.xlsx' faylı)
  2: required string token,  	        	// İstifadəçinin tokeni
  3: required string context,  	        	// İstifadəçinin konteksti
  4: string data		          	        // Sorğu məlumatı
}

struct ESResponse		                    // Sorğuya cavab
{
  1: required int code,		        		// Sorğuya cavab kodu (0 - OK, <>0 - Səhv)
  2: required string data		            // Sorğuya cavab məlumatı
}

service ESService
{
  /****************************************************************************************
   *
       ES Service
   *
  ******************************************************************************************/
  
    ESResponse esServiceRequest(1: ESRequest request)
}

