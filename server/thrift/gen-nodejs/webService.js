//
// Autogenerated by Thrift Compiler (0.12.0)
//
// DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
//
"use strict";

var thrift = require('thrift');
var Thrift = thrift.Thrift;
var Q = thrift.Q;


var ttypes = require('./web_types');
//HELPER FUNCTIONS AND STRUCTURES

var WEBService_webServiceRequest_args = function(args) {
  this.request = null;
  if (args) {
    if (args.request !== undefined && args.request !== null) {
      this.request = new ttypes.WEBRequest(args.request);
    }
  }
};
WEBService_webServiceRequest_args.prototype = {};
WEBService_webServiceRequest_args.prototype.read = function(input) {
  input.readStructBegin();
  while (true) {
    var ret = input.readFieldBegin();
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid) {
      case 1:
      if (ftype == Thrift.Type.STRUCT) {
        this.request = new ttypes.WEBRequest();
        this.request.read(input);
      } else {
        input.skip(ftype);
      }
      break;
      case 0:
        input.skip(ftype);
        break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

WEBService_webServiceRequest_args.prototype.write = function(output) {
  output.writeStructBegin('WEBService_webServiceRequest_args');
  if (this.request !== null && this.request !== undefined) {
    output.writeFieldBegin('request', Thrift.Type.STRUCT, 1);
    this.request.write(output);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var WEBService_webServiceRequest_result = function(args) {
};
WEBService_webServiceRequest_result.prototype = {};
WEBService_webServiceRequest_result.prototype.read = function(input) {
  input.readStructBegin();
  while (true) {
    var ret = input.readFieldBegin();
    var ftype = ret.ftype;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    input.skip(ftype);
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

WEBService_webServiceRequest_result.prototype.write = function(output) {
  output.writeStructBegin('WEBService_webServiceRequest_result');
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var WEBServiceClient = exports.Client = function(output, pClass) {
  this.output = output;
  this.pClass = pClass;
  this._seqid = 0;
  this._reqs = {};
};
WEBServiceClient.prototype = {};
WEBServiceClient.prototype.seqid = function() { return this._seqid; };
WEBServiceClient.prototype.new_seqid = function() { return this._seqid += 1; };

WEBServiceClient.prototype.webServiceRequest = function(request, callback) {
  this._seqid = this.new_seqid();
  if (callback === undefined) {
    var _defer = Q.defer();
    this._reqs[this.seqid()] = function(error, result) {
      if (error) {
        _defer.reject(error);
      } else {
        _defer.resolve(result);
      }
    };
    this.send_webServiceRequest(request);
    return _defer.promise;
  } else {
    this._reqs[this.seqid()] = callback;
    this.send_webServiceRequest(request);
  }
};

WEBServiceClient.prototype.send_webServiceRequest = function(request) {
  var output = new this.pClass(this.output);
  var params = {
    request: request
  };
  var args = new WEBService_webServiceRequest_args(params);
  try {
    output.writeMessageBegin('webServiceRequest', Thrift.MessageType.ONEWAY, this.seqid());
    args.write(output);
    output.writeMessageEnd();
    this.output.flush();
    var callback = this._reqs[this.seqid()] || function() {};
    delete this._reqs[this.seqid()];
    callback(null);
  }
  catch (e) {
    delete this._reqs[this.seqid()];
    if (typeof output.reset === 'function') {
      output.reset();
    }
    throw e;
  }
};
var WEBServiceProcessor = exports.Processor = function(handler) {
  this._handler = handler;
};
WEBServiceProcessor.prototype.process = function(input, output) {
  var r = input.readMessageBegin();
  if (this['process_' + r.fname]) {
    return this['process_' + r.fname].call(this, r.rseqid, input, output);
  } else {
    input.skip(Thrift.Type.STRUCT);
    input.readMessageEnd();
    var x = new Thrift.TApplicationException(Thrift.TApplicationExceptionType.UNKNOWN_METHOD, 'Unknown function ' + r.fname);
    output.writeMessageBegin(r.fname, Thrift.MessageType.EXCEPTION, r.rseqid);
    x.write(output);
    output.writeMessageEnd();
    output.flush();
  }
};
WEBServiceProcessor.prototype.process_webServiceRequest = function(seqid, input, output) {
  var args = new WEBService_webServiceRequest_args();
  args.read(input);
  input.readMessageEnd();
  this._handler.webServiceRequest(args.request);
};
