﻿/*
 *
 * CSDR System
 *
 *  Data exchange between Presentation Server (NodeJS) and backend servers (CSDR BE, CSDR AUTH)
 *
 * version 1
 * 25 January 2019
 *
 */

namespace java az.ndc.commons.thrift
 
typedef i32 integer

struct WEBRequest        			        // Request
{
	1: required integer code,		    	// Request Code (reference file - 'Service Codes.xlsx')
	3: required string socketID,			// User socket ID ("nosocketid", if absent)
	4: required string data		          	// Request Data
}



service WEBService
{
  /****************************************************************************************
   *
        NodeJS Server Data Exchange Service
   *
  ******************************************************************************************/
  
    oneway void webServiceRequest(1: WEBRequest request)
}

