const config = require("./config").config;
const AsanImzaThriftServer = require("./thrift/AsanImzaThriftServer");
let isServerUp = false;

module.exports = async io => {
//    io.on("connection", async client => {
    /**
     * Thrift Server for ASAN IMZA verification code
     */

    if (!isServerUp) {
        isServerUp = await AsanImzaThriftServer(config.thriftServerPort, io);
    }
    /** END */
//    });
};
