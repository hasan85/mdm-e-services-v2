const express = require("express");
const compression = require("compression");
const bodyParser = require("body-parser");
const methodOverride = require("method-override");
const cookieParser = require("cookie-parser");
const path = require("path");
const session = require("express-session");
const config = require("./config").config;
const expressValidator = require("express-validator");
const timeout = require("connect-timeout");
const uuid = require("uuid");
const LokiStore = require("connect-loki")(session);


module.exports = app => {
    app.use(timeout("600000s"));
    app.use(compression());
    app.use(bodyParser.urlencoded({ extended: false, limit: "50mb" }));
    app.use(bodyParser.json({ limit: "50mb" }));
    app.use(expressValidator());
    app.use(methodOverride());
    app.use(cookieParser(config.secrets.session));
    app.use(express.static(path.join(config.root, "build")));

    app.use(
        session({
            genid: function(req) {
                return uuid.v4(); // use UUIDs for session IDs
            },
            key: "express.sid",
            secret: config.secrets.session,
            cookie: { httpOnly: true, maxAge: 86400000 }, // time im ms
            resave: false,
            saveUninitialized: false,
            httpOnly: true,
            secure: true,
            ephemeral: true,
            store: new LokiStore({
                path: "session-store.db"
            })
        })
    );

    app.use((req, res, next) => {
        res.header(
            "Cache-Control",
            "no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0"
        );
        res.header("X-Frame-Options", "SAMEORIGIN");

        if (req.url.match(/^\/(css|js|img|font)\/.+/)) {
            res.setHeader("Cache-Control", "public, max-age=3600");
        }
        next();
    });

    app.use((err, req, res, next) => {
        console.dir(err.stack);
        res.json({
            success: "false",
            message: "Internal Server error (UI)"
        });
    });
};
